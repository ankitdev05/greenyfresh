<html>
<head>
	<title>Greeny Fresh India</title>
</head>
<body>
	<p>Hi User</p>
    <p>Kindly change your password in your registered email Id. You can change your password clicking on the below link. Please click on the link to reset password.</p>
    <p><b>Verification code is : {{ $url }}</b></p>
    <p><a href='{{ route('reset') }}'> {{ route('reset') }} </a></p>
    <p>Greeny Fresh India</p>
</body>
</html>