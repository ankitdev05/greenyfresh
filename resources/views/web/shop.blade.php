@extends('layouts.website')

@section('style')
    <style>
        .filter__item{border-top: 0px solid #ebebeb;}
        .product__item__pic.set-bg{height: 250px!important;background-size: contain!important; width: 100%!important;}
        .latest-product__item__pic{width: 145px;}
    </style>
@endsection

@section('content')

    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2> Shop</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span>Shop</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5">
                    <div class="sidebar">
                        <div class="sidebar__item">
                            <h4>Department</h4>
                            <ul>
                            @php $categoryLists = categorylist(); @endphp
                            @foreach($categoryLists as $categoryList)
                                <li><a href="{{ route('shop', $categoryList->alias) }}"> {{ $categoryList->descriptions[0]->title }} </a></li>
                            @endforeach
                            </ul>
                        </div>
                        <!-- <div class="sidebar__item">
                            <h4>Price</h4>
                            <div class="price-range-wrap">
                                <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                    data-min="10" data-max="540">
                                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                </div>
                                <div class="range-slider">
                                    <div class="price-input">
                                        <input type="text" id="minamount">
                                        <input type="text" id="maxamount">
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="sidebar__item">
                            <div class="latest-product__text">
                                <h4>Latest Products</h4>
                                <div class="latest-product__slider owl-carousel">

                                    <div class="latest-prdouct__slider__item">

                                    @if($latestProductListsone)
                                        @foreach($latestProductListsone as $latestProductListone)

                                        <a href="{{ route('shop/description', $latestProductListone->alias) }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src='{{ asset($latestProductListone->images[0]->image) }}' alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6> {{ $latestProductListone->descriptions[0]->name }}  - {{ $latestProductListone->option_name }}</h6>
                                                <span>Rs. {{ $latestProductListone->sale_price }}</span>
                                            </div>
                                        </a>
                                        @endforeach
                                    @endif

                                    </div>

                                    <div class="latest-prdouct__slider__item">

                                    @if($latestProductListstwo)
                                        @foreach($latestProductListstwo as $latestProductListtwo)

                                        <a href="{{ route('shop/description', $latestProductListtwo->alias) }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src='{{ asset($latestProductListtwo->images[0]->image) }}' alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6> {{ $latestProductListtwo->descriptions[0]->name }} - {{ $latestProductListtwo->option_name }} </h6>
                                                <span>Rs. {{ $latestProductListtwo->sale_price }}</span>
                                            </div>
                                        </a>
                                        @endforeach
                                    @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9 col-md-7">

                    <div class="filter__item">
                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="filter__sort">
                                    <!-- <span>Sort By</span>
                                    <select>
                                        <option value="0">Default</option>
                                        <option value="0">Default</option>
                                    </select> -->
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="filter__found">
                                    <h6><span>{{ count($productLists) }}</span> Products found</h6>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-3">
                                <div class="filter__option">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                    @if(count($productLists) > 0)

                        @foreach($productLists as $productList)

                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg='{{ asset($productList->images[0]->image) }}'>
                                        <ul class="product__item__pic__hover">
                                            <li>
                                                @if(auth()->check())
                                                    <a href="{{ route('wishlist', $productList->alias) }}"><i class="fa fa-heart"></i></a>
                                                @else
                                                    <a href="javascript:void(0)"><i class="fa fa-heart"></i></a>
                                                @endif
                                            </li>
                                            <li>
                                                @if(auth()->check())
                                                    <a href="{{ route('directaddtocart', $productList->alias) }}"><i class="fa fa-shopping-cart"></i></a>
                                                @else
                                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModalLogin"><i class="fa fa-shopping-cart"></i></a>
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="product__item__text">
                                        <h6><a href="{{ route('shop/description', $productList->alias) }}"> {{ $productList->descriptions[0]->name }} - {{ $productList->option_name }} </a></h6>
                                        <h5><del>Rs. {{ $productList->price }}</del> Rs. {{ $productList->sale_price }} </h5>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    @else
                        <div class="col-md-12">
                            <h3> No Product Found </h3>
                        </div>
                    @endif

                    </div>

                    @if(count($productLists) > 0)
                    <!-- <div class="product__pagination">
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                    </div> -->
                    @endif

                </div>
            </div>
        </div>
    </section>
    <!-- Product Section End -->


@endsection
