@extends('layouts.website')

@section('style')
    <style>
        .featured__item__pic.set-bg{height: 250px!important;background-size: contain!important; width: 100%!important;}
        .categories__item.set-bg{height: 160px!important;background-size: auto!important; background-repeat: no-repeat!important;    background-position: center!important;}
    </style>
@endsection

@section('content')

    <section class="categories">
        <div class="container">
            <div class="row">
                <div class="categories__slider owl-carousel">
                @if($advertiseLists)
                    @foreach($advertiseLists as $advertiseList)
                        <div class="col-lg-12">
                            <a href="#"> <div class="categories__item set-bg" data-setbg="{{ asset($advertiseList->image) }}">
                                {{-- <h5><a href="#">Fresh Fruit</a></h5> --}}
                            </div></a>
                        </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- Categories Section End -->

    <!-- Featured Section Begin -->
    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Featured Product</h2>
                    </div>
                    <div class="featured__controls">
                        <ul>
                            <!-- <li class="active" data-filter="*">All</li> -->
                            <!-- <li data-filter=".oranges">Oranges</li>
                            <li data-filter=".fresh-meat">Fresh Meat</li>
                            <li data-filter=".vegetables">Vegetables</li>
                            <li data-filter=".fastfood">Fastfood</li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row featured__filter1">
            @if($productLists)

                @foreach($productLists as $productList)

                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg" data-setbg='{{ asset($productList->images[0]->image) }}'>
                            <ul class="featured__item__pic__hover">
                                @if(auth()->check())
                                    <li><a href="{{ route('wishlist', $productList->alias) }}"><i class="fa fa-heart"></i></a></li>
                                @else
                                    <li><a href="javascript:void(0)"><i class="fa fa-heart"></i></a></li>
                                @endif
                                <li>
                                    @if(auth()->check())
                                        <a href="{{ route('directaddtocart', $productList->alias) }}"><i class="fa fa-shopping-cart"></i></a>
                                    @else
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#myModalLogin"><i class="fa fa-shopping-cart"></i></a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="{{ route('shop/description', $productList->alias) }}">{{ $productList->descriptions[0]->name }} - {{ $productList->option_name }}</a></h6>
                            <h5><del>Rs. {{ $productList->price }}</del> Rs. {{ $productList->sale_price }}</h5>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
            </div>
        </div>
    </section>
    <!-- Featured Section End -->

    <!-- Banner Begin -->
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="banner__pic">
                        <img src="{{ $bannerLists[1]->image }}" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="banner__pic">
                        <img src="{{ $bannerLists[2]->image }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner End -->

    <!-- Latest Product Section Begin -->
    <section class="latest-product spad">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </section>

@endsection
