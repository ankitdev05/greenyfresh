@extends('layouts.website')

@section('style')
    <style>
        .invalid-feedback{display:block;color:red;margin-bottom: 10px;}
        .contact-form form input{margin-bottom:10px;}
        a:hover, a:focus {
            text-decoration: none;
            outline: none;
            color: #7fad39;
        }
    </style>
@endsection

@section('content')

<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Login</h2>
                    <div class="breadcrumb__option">
                        <a href="{{ url('/') }}">Home</a>
                        <span>Login</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<div class="contact-form spad">
        <div class="container">
            @if(Session::has('success'))
            <div class="row">
                <div class="alert alert-danger" role="alert">
                    <strong> {{ Session::get('success') }} </strong>
                </div>
            </div>
            @endif

            <!-- <form action="#"> -->
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-lg-6 col-md-6">
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <input type="text" name="phone_no" placeholder="Your Phone Number">
                            @if($errors->has('phone_no'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('phone_no') }} </strong>
                                </div>
                            @endif
                            <input type="password" name="password" placeholder="Your Password">
                            @if($errors->has('password'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('password') }} </strong>
                                </div>
                            @endif
                            <p class="text-right"> <a href="{{ route('forgot_password') }}"> Forgot password </a> </p>
                            
                            <button type="submit" class="site-btn">Login</button>
                            <p class="text-right"> <a href="{{ route('register') }}"> Create an account? </a> </p>
                        </form>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>

@endsection
