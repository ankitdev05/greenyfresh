@extends('layouts.website')

@section('style')
@endsection

@section('content')

    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Shopping Cart</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span>Shopping Cart</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Products</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($cartArray)
                                @foreach($cartArray as $cartA)
                                    <tr>
                                        <td class="shoping__cart__item">
                                            <img src="{{ $cartA['image'] }}" alt="" width="200">
                                            <h5> {{ $cartA['name'] }} - {{ $cartA['option_name'] }}</h5>
                                        </td>
                                        <td class="shoping__cart__price">
                                            Rs. {{ $cartA['price'] }}
                                        </td>
                                        <td class="shoping__cart__quantity">
                                            <div class="quantity">
                                                <div class="cart-qty">
                                                    <input type="text" value="{{ $cartA['quantity'] }}" class="{{ $cartA['id'] }}">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="shoping__cart__total">
                                            Rs. {{ $cartA['total_price'] }}
                                        </td>
                                        <td class="shoping__cart__item__close">
                                            <a href="{{ route('remove_cart', $cartA['id']) }}"> <span class="icon_close"> </a> </span>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="{{ route('shop') }}" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
                        <!-- <a href="#" class="primary-btn cart-btn cart-btn-right"><span class="icon_loading"></span>
                            Upadate Cart</a> -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__continue">
                        <!-- <div class="shoping__discount">
                            <h5>Discount Codes</h5>
                            <form action="#">
                                <input type="text" placeholder="Enter your coupon code">
                                <button type="submit" class="site-btn">APPLY COUPON</button>
                            </form>
                        </div> -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__checkout">
                        <h5>Cart Total</h5>
                        <ul>
                            <li>Subtotal <span>Rs. {{ $subtotal }}</span></li>
                            <li>Tax <span>Rs. {{ $final_tax }}</span></li>
                            <li>Total <span>Rs. {{ $final_price }}</span></li>
                        </ul>
                        @if(count($cartArray) > 0 )
                        <a href="{{ route('checkout') }}" class="primary-btn">PROCEED TO CHECKOUT</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shoping Cart Section End -->

@endsection

@section('scripts')
<script>
(function ($) {

    var proQty1 = $('.cart-qty');
    proQty1.prepend('<span class="dec qtybtn">-</span>');
    proQty1.append('<span class="inc qtybtn">+</span>');
    proQty1.on('click', '.qtybtn', function () {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        $button.parent().find('input').val(newVal);

        var qty = newVal;
        var cid = $button.parent().find('input').attr('class');
        $.ajax({
            url:"{{route('update_cart')}}",
            method:"POST",
            data:{
            "_token": "{{ csrf_token() }}",
            id:cid,
            qty:qty,
            },
            success:function(data) {
                location.reload();
            }
        });
    });

})(jQuery);
</script>
@endsection
