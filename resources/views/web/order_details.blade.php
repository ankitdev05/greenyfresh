@extends('layouts.website')

@section('style')
    <style>
        .profile_css .col-md-12{ margin-bottom: 3%; }
        .profile_css{ border: 1px solid #d2cece; padding: 2%; }
        .profile_css a{color:#000;}
        .orderdetailscss p{ line-height: 13px; }
        .orderdetailscss .row{ border-bottom: 1px solid #a79e9e;margin-bottom: 10px; }
        .sidebaractive{color:#112198!important;font-weight: 700;}
    </style>
@endsection

@section('content')


    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Profile</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <a href="{{ route('orders') }}">Orders</a>
                            <span>Orders Details</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="blog__sidebar">

                        <div class="blog__sidebar__item">
                            <h4>Account</h4>
                            <ul>
                                <li><a href="{{ route('profile') }}">Profile</a></li>
                                <li><a href="{{ route('user_password') }}">Change Password</a></li>
                                <li><a href="{{ route('address') }}">Address</a></li>
                                <li><a class="sidebaractive" href="{{ route('orders') }}">Orders</a></li>
                                <li><a href="{{ route('wishlist_products') }}">Wishlists</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8">

                    <div class="row profile_css">

                        <div class="col-md-6">
                            <h4> Status : <span style="color:#0c3e9c"> @if($order->status == 0) Pending @elseif($order->status == 1) Accepted @endif </span></h4>                            
                            <BR>

                            <h4> Expected Delivery Date </h4>
                            <h5 style="color:#0c3e9c"> {{ date('d-M-Y', strtotime($order->created_at))  }} </h5>

                            <HR>

                            <h4>Delivery Address</h4>
                            <p> {{ $order->address  }},  {{ $order->city }}, {{ $order->state }}, {{ $order->post_code }}</p>

                            <HR>

                            <h4>Order Details</h4><BR>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Name</b>
                                </div>
                                <div class="col-md-6">
                                    {{ $order->name  }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Order number</b>
                                </div>
                                <div class="col-md-6">
                                    {{ $order->order_number  }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Order Date</b>
                                </div>
                                <div class="col-md-6">
                                    {{ date('d-M-Y', strtotime($order->created_at))  }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Order Amount</b>
                                </div>
                                <div class="col-md-6">
                                    Rs. {{ $order->total }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Payment Mode</b>
                                </div>
                                <div class="col-md-6">
                                    @if($order->payment_method == "COD") Cash On Delivery @else Online @endif
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 orderdetailscss">
                            <h4>Order Items  ({{ count($order->details) }})</h4><BR>
                        @foreach($order->details as $detail)
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="{{ asset($detail->image) }}">
                                </div>
                                <div class="col-md-9">
                                    <p>{{ $detail->name }}</p>
                                    <p><b>Quantity :</b> {{ $detail->qty }}</p>
                                    <p><b>Price :</b> {{ $detail->total_price }}</p>
                                </div>
                            </div>
                        @endforeach
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


@endsection