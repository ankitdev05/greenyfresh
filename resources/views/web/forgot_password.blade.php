@extends('layouts.website')

@section('style')
    <style>
        .invalid-feedback{display:block;color:red;margin-bottom: 10px;}
        .contact-form form input{margin-bottom:10px;}
        a:hover, a:focus {
            text-decoration: none;
            outline: none;
            color: #7fad39;
        }
    </style>
@endsection

@section('content')

<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Forgot Password</h2>
                    <div class="breadcrumb__option">
                        <a href="{{ url('/') }}">Home</a>
                        <span>Forgot Password</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<div class="contact-form spad">
        <div class="container">
            @if(Session::has('success'))
            <div class="row">
                <div class="col-md-3"></div>
                    <div class="col-lg-6 col-md-6">
                        <div class="alert alert-danger" role="alert">
                            <strong> {{ Session::get('success') }} </strong>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <!-- <form action="#"> -->
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-lg-6 col-md-6">
                        <form action="{{ route('forgot_password') }}" method="post">
                            @csrf
                            <input type="text" name="email" placeholder="Your Email Id">
                            @if($errors->has('email'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('email') }} </strong>
                                </div>
                            @endif
                            
                            <button type="submit" class="site-btn">Submit</button>
                        </form>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>

@endsection
