@extends('layouts.website')

@section('style')
    <style>
        .profile_css .col-md-12{ margin-bottom: 3%; }
        .profile_css .col-md-12 .nice-select{ width: 100%; }
        .ordercss{ border: 1px solid #d2cece; padding: 2%; }
        .orderdetailcss{ border-left: 1px solid #d2cece; }
        .profile_css a{color:#000;}
        .ordercss span,  .orderdetailcss h6{
            margin-right: 10px;
            margin-left: 10px;
        }
        .sidebaractive{color:#112198!important;font-weight: 700;}
    </style>
@endsection

@section('content')


    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Profile</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span>Orders</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="blog__sidebar">

                        <div class="blog__sidebar__item">
                            <h4>Account</h4>
                            <ul>
                                <li><a href="{{ route('profile') }}">Profile</a></li>
                                <li><a href="{{ route('user_password') }}">Change Password</a></li>
                                <li><a href="{{ route('address') }}">Address</a></li>
                                <li><a class="sidebaractive" href="{{ route('orders') }}">Orders</a></li>
                                <li><a href="{{ route('wishlist_products') }}">Wishlists</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">

                    <div class="row profile_css">

                        @if(count($orders) > 0)

                            @foreach($orders as $order)

                                <div class="col-md-12">
                                    <a href="{{ route('orders/detail', $order->order_number) }}">
                                        <div class="row ordercss">
                                            <div class="col-md-4 ordericon">
                                                <img src="{{ asset('website/greeny/order_icon.png') }}" alt="order_icon" style="width: 190px;">
                                            </div>
                                            <div class="col-md-4 orderdetailcss">
                                                <h6> <b>Order :</b> {{ $order->order_number }} </h6>
                                                <span><b> Payment Method :</b> @if($order->payment_method == "cod") Cash On Delivery @else Online @endif </span> <BR>

                                                <span><b> Status :</b>  @if($order->status == 0) Pending @elseif($order->status == 1) Accepted @endif
                                                </span> <BR>

                                                <span><b> Total Item :</b> {{ count($order->details) }}</span> 
                                            </div>
                                            <div class="col-md-4">
                                                <span><b> Payment Status :</b>  @if($order->payment_status == 1) Unpaid @else Paid @endif </span> <BR>
                                                <span><b> Arriving :</b>  {{ date('d-M-Y', strtotime($order->created_at))  }} </span> <BR>

                                                <span><b> Total :</b>  Rs. {{ $order->total }} </span>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            @endforeach
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


@endsection