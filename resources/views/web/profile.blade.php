@extends('layouts.website')

@section('style')
    <style>
        .profile_css .col-md-12{ margin-bottom: 3%; }
        .profile_css .col-md-12 .nice-select{ width: 100%; }
        .sidebaractive{color:#112198!important;font-weight: 700;}
        .invalid-feedback {display: block;}
    </style>
@endsection

@section('content')


    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Profile</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span>Profile</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="blog__sidebar">

                        <div class="blog__sidebar__item">
                            <h4>Account</h4>
                            <ul>
                                <li><a class="sidebaractive" href="{{ route('profile') }}">Profile</a></li>
                                <li><a href="{{ route('user_password') }}">Change Password</a></li>
                                <li><a href="{{ route('address') }}">Address</a></li>
                                <li><a href="{{ route('orders') }}">Orders</a></li>
                                <li><a href="{{ route('wishlist_products') }}">Wishlists</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <form action="{{ route('profile_update') }}" method="post">
                        @csrf

                        <div class="row profile_css">

                            <div class="col-md-12">
                                <input type="text" name="full_name" value="{{ auth()->user()->name }}" placeholder="Your Full Name" class="form-control">
                                @if($errors->has('full_name'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('full_name') }} </strong>
                                    </div>
                                @endif
                            </div>
                                
                            <div class="col-md-12">
                                <input type="number" name="country_code" value="{{ auth()->user()->country_code }}" placeholder="Country Code" class="form-control">
                                @if($errors->has('country_code'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('country_code') }} </strong>
                                    </div>
                                @endif
                            </div>
                            
                            <div class="col-md-12">
                                <input type="text" name="phone" value="{{ auth()->user()->phone }}" placeholder="Your Phone Number" class="form-control">
                                @if($errors->has('phone'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('phone') }} </strong>
                                    </div>
                                @endif
                            </div>
                            
                            <div class="col-md-12">
                                <input type="email" name="email" value="{{ auth()->user()->email }}" placeholder="Your Email" class="form-control">
                                @if($errors->has('email'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('email') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <select name="gender" style="width:100%" class="form-control">
                                    <option value=""> Select Gender </option>
                                    <option value="male" @if(auth()->user()->gender == "male") selected="" @endif> Male </option>
                                    <option value="female" @if(auth()->user()->gender == "female") selected="" @endif> Female </option>
                                </select>
                                @if($errors->has('gender'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('gender') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <input type="date" name="dob" value="{{ auth()->user()->dob }}" placeholder="Your Date of Birth" class="form-control">
                                @if($errors->has('dob'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('dob') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <input type="text" name="address"  value="{{ auth()->user()->address }}" placeholder="Your Address" class="form-control">
                                @if($errors->has('address'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('address') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <center>
                                    <button type="submit" class="btn btn-primary site-btn">Update</button>
                                </center>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


@endsection