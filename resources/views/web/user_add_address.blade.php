@extends('layouts.website')

@section('style')
    <style>
        .profile_css .col-md-12{ margin-bottom: 3%; }
        .profile_css .col-md-12 .nice-select{ width: 100%; }
        .sidebaractive{color:#112198!important;font-weight: 700;}
        .invalid-feedback {display: block;}
    </style>
@endsection

@section('content')


    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Profile</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <a href="{{ route('address') }}">Address</a>
                            <span>Add Address</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="blog__sidebar">

                        <div class="blog__sidebar__item">
                            <h4>Account</h4>
                            <ul>
                                <li><a href="{{ route('profile') }}">Profile</a></li>
                                <li><a href="{{ route('user_password') }}">Change Password</a></li>
                                <li><a class="sidebaractive" href="{{ route('address') }}">Address</a></li>
                                <li><a href="{{ route('orders') }}">Orders</a></li>
                                <li><a href="{{ route('wishlist_products') }}">Wishlists</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <form action="{{ route('add_address') }}" method="post">
                        @csrf

                        <div class="row profile_css">

                            <div class="col-md-12">
                                <input type="text" name="full_name" placeholder="Your Full Name" class="form-control">
                                @if($errors->has('full_name'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('full_name') }} </strong>
                                    </div>
                                @endif
                            </div>
                                
                            <div class="col-md-12">
                                <input type="text" name="phone" placeholder="Phone" class="form-control">
                                @if($errors->has('phone'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('phone') }} </strong>
                                    </div>
                                @endif
                            </div>
                            
                            <div class="col-md-12">
                                <input type="text" name="post_code" placeholder="Your post code" class="form-control">
                                @if($errors->has('post_code'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('post_code') }} </strong>
                                    </div>
                                @endif
                            </div>
                            
                            <div class="col-md-12">
                                <input type="text" name="address" placeholder="Your address" class="form-control">
                                @if($errors->has('address'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('address') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <input type="text" name="city"  value="{{ auth()->user()->city }}" placeholder="Your city" class="form-control">
                                @if($errors->has('city'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('city') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <input type="text" name="state"  value="{{ auth()->user()->state }}" placeholder="Your state" class="form-control">
                                @if($errors->has('state'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('state') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <input type="text" name="landmark"  value="{{ auth()->user()->landmark }}" placeholder="Your landmark" class="form-control">
                                @if($errors->has('landmark'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('landmark') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <center>
                                    <button type="submit" class="btn btn-primary site-btn">Save</button>
                                </center>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


@endsection