@extends('layouts.website')

@section('style')
    <style>
        .profile_css .col-md-12{ margin-bottom: 3%; }
        .profile_css .col-md-12 .nice-select{ width: 100%; }
        .profile_css a{color:#000;}
        .addresscss{ border: 1px solid #d2cece; padding: 2%; }
        .addresscss p {line-height: 8px; margin: 0 0 13px 0; }
        .addresscss.button a{margin-top: 15%; }
        .removeadd{position: absolute;right: 0;top: 0px;}
        .sidebaractive{color:#112198!important;font-weight: 700;}
    </style>
@endsection

@section('content')


    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Profile</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span>Address</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="blog__sidebar">

                        <div class="blog__sidebar__item">
                            <h4>Account</h4>
                            <ul>
                                <li><a href="{{ route('profile') }}">Profile</a></li>
                                <li><a href="{{ route('user_password') }}">Change Password</a></li>
                                <li><a class="sidebaractive" href="{{ route('address') }}">Address</a></li>
                                <li><a href="{{ route('orders') }}">Orders</a></li>
                                <li><a href="{{ route('wishlist_products') }}">Wishlists</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">

                    <div class="row profile_css">

                        @if(count($address) > 0)
                            @if(count($address) < 4)
                                <div class="col-md-6 addresscss button">
                                    <p><center><a href="{{ route('add_address') }}" class="btn btn-warning"> <i class="fa fa-home"></i> Add Address</a></center></p>
                                </div>
                            @endif

                            @foreach($address as $addr)

                                <div class="col-md-6 addresscss">
                                    <a href="{{ route('delete_address', $addr->id) }}" class="btn btn-warning removeadd"> <b>X</b> </a>

                                    <p> <b>Name :</b> {{ $addr->full_name }} </p>
                                    <p> <b>Phone :</b> {{ $addr->phone }} </p>
                                    <p> <b>Address :</b> {{ $addr->address }} </p>
                                    <p> <b>City :</b> {{ $addr->city }} </p>
                                    <p> <b>State :</b> {{ $addr->state }} </p>
                                    <p> <b>Landmark :</b> {{ $addr->landmark }} </p>
                                    <p> <b>Zip Code :</b> {{ $addr->postcode }} </p>
                                </div>

                            @endforeach
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


@endsection


