@extends('layouts.website')

@section('style')
    <style>
        .invalid-feedback{display:block!important;color:red!important;margin-bottom: 10px!important;}
        .checkout__input p{margin-bottom: 5px;}
        .checkout__input input{height: 37px;}
        .nice-select{ width: 100%; }
        .checkout__input {
            margin-bottom: 11px;
        }
        .nice-select .current{line-height: 28px;}
        span.protitle {
            width: 221px!important;
            float: right!important;
            line-height: 23px!important;
            font-weight: 600!important;
        }
        span.proprice { 
            
        }
        .checkout__order ul li span {
            float: left!important;
        }
        .checkout__order .checkout__order__subtotal {border: none!important; padding-top:0px!important;padding-bottom:0px!important;}

    </style>
@endsection

@section('content')

    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Checkout</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span>Checkout</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Checkout Section Begin -->
    <section class="checkout spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- <h6><span class="icon_tag_alt"></span> Have a coupon? <a href="#">Click here</a> to enter your code
                    </h6> -->
                </div>
            </div>
            <div class="checkout__form">
                <h4>Billing Details</h4>

                <form action="{{ route('place_order') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-8 col-md-6">
                            <div class="row">
                                @if(count($address) > 0)
                                    <div class="col-md-3">
                                        <p> <b>Select Address <span>*</span></b> </p>
                                    </div>
                                    <div class="col-md-5">
                                        <select class="form-control" name="selectedaddress" onchange="selectaddress(this.value)">
                                            <option value=""> Select Address </option>
                                            @if($address)
                                            @foreach($address as $addr)
                                                <option value="{{ $addr->id }}"> {{ $addr->address }} </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <p> -or- </p>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ route('add_address') }}" class="btn btn-primary" style="width:100%"> Add Address </a>
                                    </div>
                                @else
                                    <div class="col-md-12">
                                        <a href="{{ route('add_address') }}" class="btn btn-primary" style="width:100%"> Add Address </a>
                                    </div>
                                @endif
                            </div>
                            <HR>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Full Name<span>*</span></p>
                                        <input type="text" name="full_name" id="get_full_name" readonly="">
                                        @if($errors->has('full_name'))
                                            <div class="invalid-feedback" role="alert">
                                                <strong> {{ $errors->first('full_name') }} </strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Phone<span>*</span></p>
                                        <input type="text" name="phone" id="get_phone" readonly="">
                                        @if($errors->has('phone'))
                                            <div class="invalid-feedback" role="alert">
                                                <strong> {{ $errors->first('phone') }} </strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__input">
                                <p>Address<span>*</span></p>
                                <input type="text" name="address" placeholder="Street Address" class="checkout__input__add" id="get_address" readonly="">
                                @if($errors->has('address'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('address') }} </strong>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkout__input">
                                        <p>Town/City<span>*</span></p>
                                        <input type="text" name="city" id="get_city" readonly="">
                                        @if($errors->has('city'))
                                            <div class="invalid-feedback" role="alert">
                                                <strong> {{ $errors->first('city') }} </strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkout__input">
                                        <p>State<span>*</span></p>
                                        <input type="text" name="state" id="get_state" readonly="">
                                        @if($errors->has('state'))
                                            <div class="invalid-feedback" role="alert">
                                                <strong> {{ $errors->first('state') }} </strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkout__input">
                                        <p>Postcode / ZIP<span>*</span></p>
                                        <input type="text" name="zipcode" id="get_zipcode" readonly="">
                                        @if($errors->has('zipcode'))
                                            <div class="invalid-feedback" role="alert">
                                                <strong> {{ $errors->first('zipcode') }} </strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkout__input">
                                        <p>Landmark<span>*</span></p>
                                        <input type="text" name="landmark" id="get_landmark" readonly="">
                                        @if($errors->has('landmark'))
                                            <div class="invalid-feedback" role="alert">
                                                <strong> {{ $errors->first('landmark') }} </strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="checkout__input__checkbox">
                                <label for="acc">
                                    Create an account?
                                    <input type="checkbox" id="acc">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <p>Create an account by entering the information below. If you are a returning customer
                                please login at the top of the page</p>
                            <div class="checkout__input">
                                <p>Account Password<span>*</span></p>
                                <input type="text">
                            </div> -->
                            <!-- <div class="checkout__input__checkbox">
                                <label for="diff-acc">
                                    Ship to a different address?
                                    <input type="checkbox" id="diff-acc">
                                    <span class="checkmark"></span>
                                </label>
                            </div> -->
                            <div class="checkout__input">
                                <p>Order notes<span></span></p>
                                <input type="text" name="other_notes" 
                                    placeholder="Notes about your order, e.g. special notes for delivery.">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="checkout__order">
                                <h4>Your Order</h4>
                                <div class="checkout__order__products">Products <span>Total</span></div>
                                <ul class="productlistcss">
                                    @foreach($cartArray as $cartA)
                                    <li><span class="protitle">{{ $cartA['name'] }}</span> <span class="proprice" style="margin-top: -10px;">Rs. {{ $cartA['total_price'] }}</span></li>
                                    @endforeach
                                </ul>
                                <div style="clear:both;"></div>
                                <div class="checkout__order__subtotal" style="margin-top:30px;border-top:1px solid #e1e1e1!important">Subtotal <span>Rs. {{ $subtotal }}</span></div>
                                <div class="checkout__order__subtotal">Tax <span>Rs. {{ $final_tax }}</span></div>
                                @if($deliverycharges > 0)
                                <div class="checkout__order__total">Delivery Charges <span>Rs. {{ $deliverycharges }}</span></div>
                                @endif
                                <div class="checkout__order__total">Total <span>Rs. {{ $final_price }}</span></div>
                                <!-- <div class="checkout__input__checkbox">
                                    <label for="acc-or">
                                        Create an account?
                                        <input type="checkbox" id="acc-or">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adip elit, sed do eiusmod tempor incididunt
                                    ut labore et dolore magna aliqua.</p> -->
                                <div class="radio__input__radio">
                                    <label for="payment">
                                        <input type="radio" name="payment_mode" value="cod" id="payment" checked="">
                                        Cash On Delivery
                                        
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="radio__input__radio">
                                    <label for="paymentone">
                                        <input type="radio" name="payment_mode" value="online" id="paymentone">
                                        Credit/Debit Card
                                    </label>
                                </div>
                                <div class="radio__input__radio">
                                    <label for="paymenttwo">
                                        <input type="radio" name="payment_mode" value="paytm" id="paymenttwo">
                                        Paytm
                                    </label>
                                </div>
                                <div>
                                    <input type="hidden" name="orderamount" value="{{ $final_price }}">
                                    @if($errors->has('orderamount'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong> Minimum order amount required Rs. 200 </strong>
                                        </div>
                                    @endif
                                </div>
                                <button type="submit" class="site-btn">PLACE ORDER</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Checkout Section End -->

@endsection


@section('scripts')

<script>
    function selectaddress(id) {
        $.ajax({
            url:"{{route('get_address')}}",
            method:"POST",
            data:{
            "_token": "{{ csrf_token() }}",
            id:id,
            },
            success:function(data) {
                var result = JSON.parse(data);
                console.log(result);
                $("#get_full_name").val(result.full_name);
                $("#get_phone").val(result.phone);
                $("#get_address").val(result.address);
                $("#get_city").val(result.city);
                $("#get_state").val(result.state);
                $("#get_zipcode").val(result.postcode);
                $("#get_landmark").val(result.landmark);
            }
        });
    }
</script>

@endsection