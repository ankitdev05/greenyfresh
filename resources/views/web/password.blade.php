@extends('layouts.website')

@section('style')
    <style>
        .profile_css .col-md-12{ margin-bottom: 3%; }
        .profile_css .col-md-12 .nice-select{ width: 100%; }
        .blog__sidebar { padding-top: 0px; }
        .sidebaractive{color:#112198!important;font-weight: 700;}
        .invalid-feedback {display: block;}
    </style>
@endsection

@section('content')


    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Profile</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span>Change Password</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="blog__sidebar">

                        <div class="blog__sidebar__item">
                            <h4>Account</h4>
                            <ul>
                                <li><a href="{{ route('profile') }}">Profile</a></li>
                                <li><a class="sidebaractive" href="{{ route('user_password') }}">Change Password</a></li>
                                <li><a href="{{ route('address') }}">Address</a></li>
                                <li><a href="{{ route('orders') }}">Orders</a></li>
                                <li><a href="{{ route('wishlist_products') }}">Wishlists</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <form action="{{ route('update_password') }}" method="post">
                        @csrf

                        <div class="row profile_css">

                            <div class="col-md-12">
                                <input type="text" name="old_password" placeholder="Your Old Password" class="form-control">
                                @if($errors->has('old_password'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('old_password') }} </strong>
                                    </div>
                                @endif
                            </div>
                                
                            <div class="col-md-12">
                                <input type="password" name="new_password" placeholder="Your New Password" class="form-control">
                                @if($errors->has('new_password'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong> {{ $errors->first('new_password') }} </strong>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                <center>
                                    <button type="submit" class="btn btn-primary site-btn">Update</button>
                                </center>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


@endsection