@extends('layouts.website')

@section('style')
    <style>

    </style>
@endsection

@section('content')
    
    <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2> {{ $page->descriptions[0]->title }} </h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span> {{ $page->descriptions[0]->title }} </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {!! $page->descriptions[0]->content !!}
                </div>
            </div>
        </div>
    </section>

@endsection
