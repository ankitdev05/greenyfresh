@extends('layouts.website')

@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .card-header { background: #7fad39; color: #fff; }
        .card-body { font-size: 15px;line-height: 19px;margin: 32px 0px 15px 0;  }
    </style>
@endsection

@section('content')

    <div id="app">
        <main class="py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-3 col-md-offset-6">
                        @if($message = Session::get('error'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>Error!</strong> {{ $message }}
                            </div>
                        @endif
                        @if($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade {{ Session::has('success') ? 'show' : 'in' }}" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>Success!</strong> {{ $message }}
                            </div>
                        @endif
                        <div class="card card-default">
                            <div class="card-header">
                                Review Your Order & Complete Checkout
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="checkout__order">
                                            <h4>Your Order</h4>
                                            <div class="checkout__order__products">Products <span>Total</span></div>
                                            <div class="checkout__order__subtotal">Subtotal <span>Rs. {{ $subtotal }} </span></div>
                                            @if($deliverycharges > 0)
                                            <div class="checkout__order__total">Delivery Charge <span>Rs. {{ $deliverycharges }} </span></div>
                                            @endif
                                            <div class="checkout__order__total">Total <span>Rs. {{ $final_price }} </span></div>

                                        </div>
                                    </div>
                                </div>

                                <HR>
                                <center>
                                
                                <form action="{{ route('payment') }}" method="POST" >
                                    @csrf
                                    
                                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                                            data-key="{{ env('RAZOR_KEY') }}"
                                            data-amount="{{ $final_price * 100 }}"
                                            data-currency="INR"
                                            data-buttontext="Pay Now"
                                            data-name="NiceSnippets"
                                            data-description="Rozerpay"
                                            data-image="{{ asset('/image/nice.png') }}"
                                            data-prefill.name="{{ $address->full_name }}"
                                            data-prefill.email="{{ auth()->user()->email }}"
                                            data-theme.color="#F37254">
                                    </script>
                                </form>

                                </center>
                                <HR>
                                <p>By submiting this order you are agreeing to our universal billing agreement, and terms of service. If you have any questions about our products or services please contact us before placing this order.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>

@endsection


@section('scripts')

@endsection