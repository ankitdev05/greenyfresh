<section class="hero @if(!Request::is('/')) hero-normal @endif ">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>All departments</span>
                        </div>
                        <ul>
                        @php $categoryLists = categorylist(); @endphp
                        @foreach($categoryLists as $categoryList)
                            <li><a href="{{ route('shop', $categoryList->alias) }}"> {{ $categoryList->descriptions[0]->title }} </a></li>
                        @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="{{ route('search') }}" method="get">
                                <div class="hero__search__categories">
                                    <span style="color: #bfb9b9;">What do yo u need?</span>
                                    <!-- <span class="arrow_carrot-down"></span> -->
                                </div>
                                <input type="text" name="key" placeholder="">
                                <button type="submit" class="site-btn">SEARCH</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>+91-9817138320</h5>
                                <span>support 24/7 time</span>
                            </div>
                        </div>
                    </div>

                    @if(Request::is('/'))

                    <div class="hero__item set-bg" data-setbg="{{ asset(bannerlist()->image) }}">
                        <div class="hero__text">
                            <span>FRUIT FRESH</span>
                            <h2>Vegetable <br />100% Organic</h2>
                            <p>Free Pickup and Delivery Available</p>
                            <a href="{{ route('shop') }}" class="primary-btn">SHOP NOW</a>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </section>
