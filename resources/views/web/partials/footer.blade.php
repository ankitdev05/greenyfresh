    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="{{ url('/') }}"><img src="{{ asset('website/greeny/website_logo_solid_background.png') }}" alt="" style="width:200px; height:100px;"></a>
                        </div>
                        <ul>
                            <li><b>Address:</b> Greenyfresh traders </li>
                            <li> Ground floor, Pawan mega mall, </li>
                            <li> Subhash chowk, Sonepat, Haryana 131001 </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Useful Links</h6>
                        <ul>
                            <li><a href="{{ route('shop') }}">Our Shop</a></li>
                            @php $footerPages = footerlist(); @endphp
                            @foreach($footerPages as $footerPage)
                                <li><a href="{{url('/')}}/{{ $footerPage->alias }}"> {{ $footerPage->descriptions[0]->title }} </a></li>
                            @endforeach
                        </ul>
                        <!-- <ul>
                            <li><a href="#">Who We Are</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Projects</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Innovation</a></li>
                            <li><a href="#">Testimonials</a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Join On Social links</h6>

                        <div class="footer__widget__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            {{-- <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a> --}}
                        </div>
                        
                        <h6 style="margin-top: 20px;"> WhatsApp On </h6>
                        <ul>
                            <li> +91-9817138320</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                        <div class="footer__copyright__payment"><img src="{{ asset('website/img/payment-item.png') }}" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


<!-- Modal -->
<div id="myModalLogin" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Login</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-lg-10 col-md-10">
                    <form action="{{ route('login') }}" method="post" class="login_popup">
                        @csrf
                        <input type="text" name="phone_no" placeholder="Your Phone Number" class="form-control">
                        @if($errors->has('phone_no'))
                            <div class="invalid-feedback" role="alert">
                                <strong> {{ $errors->first('phone_no') }} </strong>
                            </div>
                        @endif
                        <input type="password" name="password" placeholder="Your Password" class="form-control">
                        @if($errors->has('password'))
                            <div class="invalid-feedback" role="alert">
                                <strong> {{ $errors->first('password') }} </strong>
                            </div>
                        @endif
                        <button type="submit" class="site-btn">Login</button>
                    </form>
                </div>
            </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>

  </div>
</div>
