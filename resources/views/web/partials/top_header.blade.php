<div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="{{ url('/') }}"><img src="{{ asset('website/greeny/website_logo_solid_background.png') }}" alt=""></a>
        </div>
        <div class="humberger__menu__cart">
            @php $cartheaderr = cartheader(); @endphp
            <ul>
                <li><a href="{{ route('wishlist_products') }}"><i class="fa fa-heart"></i> </a></li>
                <li><a href="{{ route('cart') }}"><i class="fa fa-shopping-bag"></i> <span>{{ $cartheaderr['cartTotal'] }}</span></a></li>
            </ul>
            <div class="header__cart__price">item: <span> Rs {{ $cartheaderr['total'] }} </span></div>
        </div>
        <div class="humberger__menu__widget">
            <div class="header__top__right__language">
                <!-- <img src="{{ asset('website/img/language.png') }}" alt="">
                <div>English</div>
                <span class="arrow_carrot-down"></span>
                <ul>
                    <li><a href="#">Spanis</a></li>
                    <li><a href="#">English</a></li>
                </ul> -->
            </div>
            <div class="header__top__right__auth">
                @if(Auth::check())
                <a href="{{ route('login') }}"><i class="fa fa-user"></i> Account </a>
                <a href="{{ route('logout') }}"><i class="fa fa-user"></i> Logout </a>
                @else
                    <a href="{{ route('login') }}"><i class="fa fa-user"></i> Login</a>
                @endif
            </div>
        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li class="active"><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ route('shop') }}">Shop</a></li>
                @php $headerPages = headerlist(); @endphp
                @foreach($headerPages as $headerPage)
                    <li><a href="{{url('/')}}/{{ $headerPage->alias }}"> {{ $headerPage->descriptions[0]->title }} </a></li>
                @endforeach
                {{-- <li><a href="./contact.html">Contact</a></li> --}}
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            {{-- <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a> --}}
        </div>
        <div class="humberger__menu__contact">
            <ul>
                {{-- <li><i class="fa fa-envelope"></i> hello@gmail.com</li> --}}
                <li>Free Shipping for all Order above Rs. 300</li>
            </ul>
        </div>
    </div>
