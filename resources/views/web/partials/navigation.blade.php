<header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__left">
                            <ul>
                                {{-- <li><i class="fa fa-envelope"></i> hello@gmail.com</li> --}}
                                <li>Free Shipping for all Order above Rs. 300</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__right">
                            <div class="header__top__right__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                {{-- <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a> --}}
                            </div>
                            <div class="header__top__right__language">
                                @if(Auth::check())
                                    <a href="{{ route('profile') }}"><i class="fa fa-user"></i> Account</a>
                                @endif
                            </div>

                            <div class="header__top__right__auth">
                                @if(Auth::check())
                                    <a href="{{ route('logout') }}"><i class="fa fa-user"></i> Logout</a>
                                @else
                                    <a href="{{ route('login') }}"><i class="fa fa-user"></i> Login</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="{{ url('/') }}"><img src="{{ asset('website/greeny/website_logo_solid_background.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li class="active"><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ route('shop') }}">Shop</a></li>
                            
                            @php $headerPages = headerlist(); @endphp
                            @foreach($headerPages as $headerPage)
                                <li><a href="{{url('/')}}/{{ $headerPage->alias }}"> {{ $headerPage->descriptions[0]->title }} </a></li>
                            @endforeach

                            {{-- <li><a href="{{ route('contact') }}">Contact</a></li> --}}
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                    @php $cartheader = cartheader(); @endphp
                        <ul>
                            <li><a href="{{ route('wishlist_products') }}"><i class="fa fa-heart"></i> </a></li>
                            <li><a href="{{ route('cart') }}"><i class="fa fa-shopping-bag"></i> <span>{{ $cartheader['cartTotal'] }}</span></a></li>
                        </ul>
                        <div class="header__cart__price">item: <span>Rs {{ $cartheader['total'] }}</span></div>
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
