@extends('layouts.api')

@section('style')
    <style>

    </style>
@endsection

@section('content')

    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {!! $page->descriptions[0]->content !!}
                </div>
            </div>
        </div>
    </section>

@endsection
