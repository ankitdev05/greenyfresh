@extends('layouts.website')

@section('style')
    <style>
        .profile_css .col-md-12{ margin-bottom: 3%; }
        .profile_css .col-md-12 .nice-select{ width: 100%; }
        .sidebaractive{color:#112198!important;font-weight: 700;}
    </style>
@endsection

@section('content')


    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Profile</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ url('/') }}">Home</a>
                            <span>Wishlists</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <div class="blog__sidebar">

                        <div class="blog__sidebar__item">
                            <h4>Account</h4>
                            <ul>
                                <li><a href="{{ route('profile') }}">Profile</a></li>
                                <li><a href="{{ route('user_password') }}">Change Password</a></li>
                                <li><a href="{{ route('address') }}">Address</a></li>
                                <li><a href="{{ route('orders') }}">Orders</a></li>
                                <li><a class="sidebaractive" href="{{ route('wishlist_products') }}">Wishlists</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <section class="shoping-cart spad">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="shoping__cart__table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th class="shoping__product">Products</th>
                                                <th>Name</th>
                                                <th>Price</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if($wishlists)
                                            @foreach($wishlists as $wish)
                                                <tr>
                                                    <td class="shoping__cart__item" style="width: 240px;">
                                                        <a href="{{ route('shop/description', $wish->product->alias) }}"><img src="{{ $wish->product->images[0]->image }}" alt="" width="100"></a>
                                                    </td>
                                                    <td class="shoping__cart__quantity" style="width: 288px;">
                                                        <a href="{{ route('shop/description', $wish->product->alias) }}"><h5> {{ $wish->product->descriptions[0]->name }} </h5></a>
                                                    </td>
                                                    <td class="shoping__cart__price">
                                                        Rs. {{ $wish->product->sale_price }}
                                                    </td>
                                                    <td class="shoping__cart__item__close">
                                                        <a href="{{ route('wishlist', $wish->product->alias) }}"> <span class="icon_close"> </a> </span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


@endsection