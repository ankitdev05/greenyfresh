@extends('layouts.website')

@section('style')
    <style>
        .nice-select{width: 100%;margin-bottom: 10px; }
        .invalid-feedback{display:block;color:red;margin-bottom: 10px;}
        .contact-form form input{margin-bottom:10px;}
        a:hover, a:focus {
            text-decoration: none;
            outline: none;
            color: #7fad39;
        }
    </style>
@endsection

@section('content')

<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="{{ asset('website/img/breadcrumb.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Register</h2>
                    <div class="breadcrumb__option">
                        <a href="{{ url('/') }}">Home</a>
                        <span>Register</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<div class="contact-form spad">
        <div class="container">
            <!-- <div class="row">
                <div class="col-lg-12">
                    <div class="contact__form__title">
                         <h2>Leave Message</h2>
                    </div>
                </div>
            </div> -->
            <!-- <form action="#"> -->
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-lg-6 col-md-6">
                        <form action="{{ route('register') }}" method="post">
                            @csrf
                            <input type="text" name="name" placeholder="Your Full Name">
                            @if($errors->has('name'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('name') }} </strong>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="number" name="country_code" placeholder="Country Code" style="padding-left: 7px;">
                                    @if($errors->has('country_code'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong> {{ $errors->first('country_code') }} </strong>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="phone" placeholder="Your Phone Number">
                                    @if($errors->has('phone'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong> {{ $errors->first('phone') }} </strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <input type="email" name="email" placeholder="Your Email">
                            @if($errors->has('email'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('email') }} </strong>
                                </div>
                            @endif
                            <select name="gender" style="width:100%">
                                <option value=""> Select Gender </option>
                                <option value="male"> Male </option>
                                <option value="female"> Female </option>
                            </select>
                            @if($errors->has('gender'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('gender') }} </strong>
                                </div>
                            @endif
                            <input type="date" name="dob" placeholder="Your Date of Birth">
                            @if($errors->has('dob'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('dob') }} </strong>
                                </div>
                            @endif
                            <input type="text" name="address" placeholder="Your Address">
                            @if($errors->has('address'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('address') }} </strong>
                                </div>
                            @endif
                            <input type="password" name="password" placeholder="Your Password">
                            @if($errors->has('password'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('password') }} </strong>
                                </div>
                            @endif
                            <input type="password" name="confirm_password" placeholder="Your Confirm Password">
                            @if($errors->has('confirm_password'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('confirm_password') }} </strong>
                                </div>
                            @endif
                            <button type="submit" class="site-btn">Register</button>
                            <p class="text-right"> <a href="{{ route('login') }}"> Already have an account? </a> </p>
                        </form>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>

@endsection
