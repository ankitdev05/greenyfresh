@extends('layouts.administrator')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banner Add</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('administrator.banner') }}">Banner</a></li>
              <li class="breadcrumb-item active">Banner Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong> {{ Session::get('success') }} </strong>
            </div>
        @endif

      <form action="{{ route('administrator.banner_add') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-10">
            <div class="card card-secondary">
                <div class="card-header">
                <h3 class="card-title">Banner</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <!-- <i class="fas fa-minus"></i>--></button>
                </div>
                </div>
                <div class="card-body">

                <div class="form-group">
                    <label for="inputImage">Image</label>
                    <div class="custom-file">
                        <input type="file" name="image" id="customFile" class="custom-file-input">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    @if($errors->has('image'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('image') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputUrl">URL</label>
                    <input type="text" name="url" id="inputUrl" class="form-control">
                    @if($errors->has('url'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('url') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputTarget">Target</label>
                    <select name="target" id="inputTarget" class="form-control">
                        <option> Select Target </option>
                        <option value="new"> New Window </option>
                        <option value="same"> Same Window </option>
                    </select>
                    @if($errors->has('target'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('target') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputSort">Sort</label>
                    <select name="sort" id="inputSort" class="form-control">
                        <option> Select Sort </option>
                        <option value="1"> 1 </option>
                        <option value="2"> 2 </option>
                        <option value="3"> 3 </option>
                    </select>
                    @if($errors->has('sort'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('sort') }} </strong>
                        </div>
                    @endif
                </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-10">
            <a href="{{ route('administrator.banner') }}" class="btn btn-secondary">Cancel</a>
            <input type="submit" value="Create" class="btn btn-success float-right">
            </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>

  @endsection
