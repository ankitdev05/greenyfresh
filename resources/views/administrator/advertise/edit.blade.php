@extends('layouts.administrator')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Advertise Edit</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('administrator.advertise') }}">Advertise</a></li>
              <li class="breadcrumb-item active">Advertise Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong> {{ Session::get('success') }} </strong>
            </div>
        @endif

      <form action="{{ route('administrator.advertise_edit', $advertise->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-10">
            <div class="card card-secondary">
                <div class="card-header">
                <h3 class="card-title">Advertise</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <!-- <i class="fas fa-minus"></i>--></button>
                </div>
                </div>
                <div class="card-body">

                <div class="form-group">
                    <label for="inputImage">Image</label>
                    <div class="custom-file">
                        <input type="file" name="image" id="customFile" class="custom-file-input">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    @if($errors->has('image'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('image') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputUrl">URL</label>
                    <input type="text" name="url" value="{{ $advertise->url }}" id="inputUrl" class="form-control">
                    @if($errors->has('url'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('url') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputTarget">Target</label>
                    <select name="target" id="inputTarget" class="form-control">
                        <option> Select Target </option>
                        <option value="new" @if($advertise->target == "new") selected @endif> New Window </option>
                        <option value="same" @if($advertise->target == "same") selected @endif> Same Window </option>
                    </select>
                    @if($errors->has('target'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('target') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputSort">Sort</label>
                    <select name="sort" id="inputSort" class="form-control">
                        <option> Select Sort </option>
                        <option value="1" @if($advertise->sort == 1) selected @endif> 1 </option>
                        <option value="2" @if($advertise->sort == 2) selected @endif> 2 </option>
                        <option value="3" @if($advertise->sort == 3) selected @endif> 3 </option>
                        <option value="4" @if($advertise->sort == 4) selected @endif> 4 </option>
                        <option value="5" @if($advertise->sort == 5) selected @endif> 5 </option>
                        <option value="6" @if($advertise->sort == 6) selected @endif> 6 </option>
                        <option value="7" @if($advertise->sort == 7) selected @endif> 7 </option>
                        <option value="8" @if($advertise->sort == 8) selected @endif> 8 </option>
                        <option value="9" @if($advertise->sort == 9) selected @endif> 9 </option>
                        <option value="10" @if($advertise->sort == 10) selected @endif> 10 </option>
                    </select>
                    @if($errors->has('sort'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('sort') }} </strong>
                        </div>
                    @endif
                </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-10">
            <a href="{{ route('administrator.advertise') }}" class="btn btn-secondary">Cancel</a>
            <input type="submit" value="Update" class="btn btn-success float-right">
            </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>

  @endsection
