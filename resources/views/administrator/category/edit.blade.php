@extends('layouts.administrator')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Category Edit</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('administrator.category') }}">Category</a></li>
              <li class="breadcrumb-item active">Category Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong> {{ Session::get('success') }} </strong>
            </div>
        @endif

      <form action="{{ route('administrator.category_edit', $category->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-10">
            <div class="card card-secondary">
                <div class="card-header">
                <h3 class="card-title">Category</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <!-- <i class="fas fa-minus"></i>--></button>
                </div>
                </div>
                <div class="card-body">
                <div class="form-group">
                    <label for="inputTitle">Title</label>
                    <input type="text" name="title" value="{{ $category->descriptions[0]->title }}" id="inputTitle" class="form-control">
                    @if($errors->has('title'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('title') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputKeyword">Keyword</label>
                    <input type="text" name="keyword" value="{{ $category->descriptions[0]->keyword }}" id="inputKeyword" class="form-control">
                    @if($errors->has('keyword'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('keyword') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputDescription">Description</label>
                    <input type="text" name="description" value="{{ $category->descriptions[0]->description }}" id="inputDescription" class="form-control">
                    @if($errors->has('description'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('description') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputImage">Image</label>
                    <div class="custom-file">
                        <input type="file" name="image" id="customFile" class="custom-file-input">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    @if($errors->has('image'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('image') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputSort">Sort</label>
                    <input type="number" name="sort" value="{{ $category->sort }}" id="inputSort" class="form-control">
                    @if($errors->has('sort'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('sort') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputStatus">Status</label>
                    <select name="status" class="form-control" id="inputStatus">
                        <option value="1" @if($category->status == 1) selected @endif> Active </option>
                        <option value="2" @if($category->status == 2) selected @endif> InActive </option>
                    </select>
                </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-10">
            <a href="{{ route('administrator.category') }}" class="btn btn-secondary">Cancel</a>
            <input type="submit" value="Update" class="btn btn-success float-right">
            </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>

  @endsection
