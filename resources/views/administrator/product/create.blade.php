@extends('layouts.administrator')

@section('style')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
    <style>
        .variantNo .col-md-2{float:left;}
        .variantYes .col-md-2{float:left;}
        .variantNo{clear:both;}
        .variantYes{clear:both;}
    </style>
@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Product Add</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('administrator.product') }}">Product</a></li>
              <li class="breadcrumb-item active">Product Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong> {{ Session::get('success') }} </strong>
            </div>
        @endif

      <form action="{{ route('administrator.product_add') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-10">
            <div class="card card-secondary">
                <div class="card-header">
                <h3 class="card-title">Product</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <!-- <i class="fas fa-minus"></i>--></button>
                </div>
                </div>
                <div class="card-body">
                <div class="form-group">
                    <label for="inputTitle">Name</label>
                    <input type="text" name="name" id="inputTitle" class="form-control" max="200">
                    @if($errors->has('name'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('name') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputKeyword">Keyword</label>
                    <input type="text" name="keyword" id="inputKeyword" class="form-control" max="200">
                    @if($errors->has('keyword'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('keyword') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputDescription">Description</label>
                    <input type="text" name="description" id="inputDescription" class="form-control" max="300">
                    @if($errors->has('description'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('description') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputContent">Content</label>
                    <textarea name="content" id="inputContent" class="form-control"></textarea>
                    @if($errors->has('content'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('content') }} </strong>
                        </div>
                    @endif
                </div>

                <BR>
                <HR>
                <HR>
                <BR>

                <div class="form-group">
                    <label for="inputDescription">Select Category</label>
                    <select name="category" class="form-control">
                        <option value=""> Select </option>
                        @foreach($catData as $catD)
                            <option value="{{ $catD->id }}"> {{ $catD->descriptions[0]->title }} </option>
                        @endforeach
                    </select>
                    @if($errors->has('category'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('category') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputImage">Image</label>
                    <div class="custom-file">
                        <input type="file" name="image" id="customFile" class="custom-file-input">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    @if($errors->has('image'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('image') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputDescription">Select Attribute</label>
                    <select name="attribute" onchange="dropchange(this.value)" class="form-control parentAttribute">
                        <option value="">Select Attribute</option>
                        @foreach($attributeData as $attributeD)
                            <option value="{{ $attributeD->id }}"> {{ $attributeD->name }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="inputsku">Options</label>
                    <select type="text" name="option" class="form-control optionlisting" style="font-size: 13px;">
                        <option value=""> Select Option </option>
                    </select>
                </div>

                <div class="row variantNo">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="inputsku">SKU</label>
                            <input type="text" name="sku" id="inputsku" class="form-control" max="10">
                            @if($errors->has('sku'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('sku') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="inputPrice">Price</label>
                            <input type="text" name="price" id="inputPrice" class="form-control">
                            @if($errors->has('price'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('price') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="inputSalePrice">Sale Price</label>
                            <input type="text" name="sale_price" id="inputSalePrice" class="form-control">
                            @if($errors->has('sale_price'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('sale_price') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="inputStock">Stock</label>
                            <input type="number" name="stock" id="inputStock" class="form-control">
                            @if($errors->has('stock'))
                                <div class="invalid-feedback" role="alert">
                                    <strong> {{ $errors->first('stock') }} </strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="" style="clear:both;"></div>

                <HR>

                <div class="form-group">
                    <label for="inputDescription">Select Tax</label>
                    <select name="tax" class="form-control">
                        <option value="">Select</option>
                        @foreach($taxData as $taxD)
                            <option value="{{ $taxD->id }}"> {{ $taxD->name }} - {{ $taxD->value }} </option>
                        @endforeach
                    </select>
                    @if($errors->has('tax'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('tax') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="inputType">Type</label>
                    <select name="type" id="inputType" class="form-control">
                        <option value="">Select</option>
                        <option value="1">Normal</option>
                        <option value="2">New</option>
                        <option value="3">Hot</option>
                    </select>
                    @if($errors->has('type'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('type') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group clearfix">
                    <label for="inputStatus">Status</label>
                    <div class="icheck-success d-inline">
                        <input type="checkbox" checked="" name="status" value="1" id="checkboxSuccess1" style="width: 20px;height: 20px;margin-left: 40px;">
                        <label for="checkboxSuccess1">
                        </label>
                    </div>
                    @if($errors->has('status'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('status') }} </strong>
                        </div>
                    @endif
                </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        </div>
        
        <div class="" style="clear:both;"></div>

        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('administrator.product') }}" class="btn btn-secondary">Cancel</a>
            </div>
            <div class="col-6">
                <input type="submit" value="Create" class="btn btn-success float-right">
            </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>

  @endsection

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
    $(function () {
        // Summernote
        $('#inputContent').summernote();
    })
    </script>

    <script type="text/javascript">
        function dropchange(valu) {

            var seletedValue = valu;
            $.ajax({
                url:"{{route('administrator.attribute_option_ajax')}}",
                method:"POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    selection:seletedValue,
                },
                success:function(data) {
                    var result = JSON.parse(data);
                    $('.optionlisting').html(result);
                }
            });
        }
    </script>
@endsection
