@extends('layouts.administrator')

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Products</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Products</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="box-header with-border">
                    <div class="float-right">
                        <div class="btn-group pull-right" style="margin-right: 10px">
                            <div class="menu-right">
                                <a href="{{ route('administrator.product_add') }}" class="btn  btn-success  btn-flat" title="New" id="button_create_new">
                                    <i class="fa fa-plus"></i><span class="hidden-xs">Add new</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="float-left">
                        <div class="menu-left">

                        </div>

                        <div class="menu-left">
                        <a class="btn btn-flat btn-primary grid-refresh" title="Refresh"><i class="fa fa-refresh"></i><span class="hidden-xs"> Refresh</span></a>
                        </div>
                    </div>
                </div>

                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <strong> {{ Session::get('success') }} </strong>
                    </div>
                @endif

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Image</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if($productLists)
                    @foreach($productLists as $productList)
                        <tr>
                            <td> P-{{ $productList->id }}</td>
                            <td> <img src="{{ asset($productList->images[0]->image) }}" width="100"> </td>
                            <td> {{ $productList->sku }} </td>
                            <td> {{ $productList->descriptions[0]->name }} </td>
                            <td> {{ $productList->categories[0]->descriptions[0]->title }} </td>
                            <td> {{ $productList->price }} </td>
                            <td> {{ $productList->sale_price }} </td>
                            <td> @if($productList->status == 1) Active @else Inactive @endif</td>
                            <td>
                                <a onclick="return confirm('Are you sure?')" href="{{ route('administrator.product_delete', $productList->id) }}" class="btn btn-flat btn-danger grid-trash" title="Delete"><i class="fa fa-trash-o"></i><span class="hidden-xs"> Delete</span></a>

                                <a href="{{ route('administrator.product_edit', $productList->id) }}" class="btn btn-flat btn-warning" title="Edit"><i class="fa fa-edit-o"></i><span class="hidden-xs"> Edit</span></a>
                            </td>
                        </tr>
                    @endforeach
                  @endif
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Image</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  @endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
            });
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });
    </script>
@endsection
