@extends('layouts.administrator')

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Order Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('administrator.orders') }}">Orders</a></li>
              <li class="breadcrumb-item active">Order Details</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fas fa-globe"></i> Order Status: @if($orderList->status == 0) Pending @elseif($orderList->status == 1) Accepted @endif
                    <small class="float-right">Date: {{ date('d-M-Y', strtotime($orderList->created_at)) }}</small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  Customer
                  <address>
                    <strong> {{ $orderList->user->name }} </strong><br>
                    Address: {{ $orderList->user->address }}<br>
                    Gender: {{ $orderList->user->gender }}<br>
                    Phone: {{ $orderList->user->phone }}<br>
                    Email: {{ $orderList->user->email }}<br>
                    Login Type: {{ $orderList->user->device_type }}<br>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  Shipping Address Details
                  <address>
                    <strong> {{ $orderList->user->name }} </strong><br>
                    {{ $orderList->address }}<br>
                    City: {{ $orderList->city }}<br>
                    State: {{ $orderList->state }}<br>
                    Landmark: {{ $orderList->landmark }}<br>
                    Zipcode: {{ $orderList->post_code }}<br>
                    Phone: {{ $orderList->phone }}<br>
                    Email: {{ $orderList->user->email }}
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice #{{ $orderList->id }}{{ $orderList->order_number }}</b><br>
                  <br>
                  <b>Order ID:</b> {{ $orderList->order_number }}<br>
                  <b>Order Date:</b> {{ date('d-M-Y', strtotime($orderList->created_at)) }}<br>
                  <b>Payment Mode:</b> @if($orderList->payment_status == 1) Unpaid @else Paid @endif<br>
                  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Product</th>
                      <th>Serial #</th>
                      <th>Qty</th>
                      <th>Price</th>
                      <th>Tax</th>
                      <th>Total Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orderList->details as $details)
                    <tr>
                      <td> {{ $details->name }} </td>
                      <td> {{ $details->sku }} </td>
                      <td> {{ $details->qty }} </td>
                      <td> {{ $details->price }} </td>
                      <td> {{ $details->tax }} </td>
                      <td> {{ $details->total_price }} </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                  <p class="lead">Payment Methods:</p>

                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    @if($orderList->payment_method == "COD") Cash On Delivery @else Online @endif
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Total Amount</p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td> Rs. {{ $orderList->subtotal }} </td>
                      </tr>
                      <tr>
                        <th>Tax </th>
                        <td> Rs. {{ $orderList->tax }} </td>
                      </tr>
                      <tr>
                        <th>Shipping:</th>
                        <td>Rs. {{ $orderList->shipping }} </td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td> Rs. {{ $orderList->total }} </td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  {{-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a> --}}

                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  @endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
            });
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });
    </script>
@endsection
