@extends('layouts.administrator')

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <style>
      .example1 thead tr th,.example1 tfoot tr th{ font-size: 12px!important; }
    </style>
@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Orders</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Orders</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="box-header with-border">
                    <div class="float-right">
                        <div class="btn-group pull-right" style="margin-right: 10px">
                            <div class="menu-right">
                                
                            </div>
                        </div>
                    </div>
                    <div class="float-left">
                        <div class="menu-left">

                        </div>

                        <div class="menu-left">
                        
                        </div>
                    </div>
                </div>

                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <strong> {{ Session::get('success') }} </strong>
                    </div>
                @endif

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Order Number</th>
                    <th>Items</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Payment Method</th>
                    <th>Order Date</th>
                    <th>Status</th>
                    <th>Order Process</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if($orderLists)
                    @foreach($orderLists as $orderList)
                        <tr>
                            <td> {{ $orderList->order_number }}</td>
                            <td> {{ count($orderList->details) }}</td>
                            <td> {{ $orderList->user->name }}</td>
                            <td> {{ $orderList->user->phone }}</td>
                            <td> @if($orderList->payment_method == "cod") Cash On Delivery @else Online @endif</td>
                            <td> {{ date('d-M-Y', strtotime($orderList->created_at)) }} </td>
                            <td> 
                                @if($orderList->status == 0) 
                                  Pending
                                @elseif($orderList->status == 1) 
                                  Accepted
                                @elseif($orderList->status == 2) 
                                  Shipped
                                @elseif($orderList->status == 3) 
                                  Delivered
                                @endif
                            </td>
                            <td>
                                <select name="order_status" class="{{$orderList->order_number}}" onchange='statuschange("{{$orderList->order_number}}", this.value)'>
                                  <option value=""> Select </option>
                                  @if($orderList->status == 0)
                                    <option value="1"> Accepted </option>
                                    <option value="2"> Shipped </option>
                                    <option value="3"> Delivered </option>
                                  @elseif($orderList->status == 1)
                                    <option value="2"> Shipped </option>
                                    <option value="3"> Delivered </option>
                                  @elseif($orderList->status == 2)
                                    <option value="3"> Delivered </option>
                                  @endif
                                </select>
                            </td>
                            <td> <a href="{{ route('administrator.order_detail', $orderList->id) }}"> <i class="far fa-eye"></i> </a> </td>
                            
                        </tr>
                    @endforeach
                  @endif
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Order Number</th>
                    <th>Items</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Payment Method</th>
                    <th>Order Date</th>
                    <th>Status</th>
                    <th>Order Process</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  @endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
            });
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });

        function statuschange(id, status) {

          $.ajax({
              url:"{{route('administrator.order_status_change')}}",
              method:"POST",
              data:{
              "_token": "{{ csrf_token() }}",
              id:id,
              status:status,
              },
              success:function(data) {
                  location.reload();
              }
          });
        }
    </script>
@endsection
