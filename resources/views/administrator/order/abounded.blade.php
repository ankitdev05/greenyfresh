@extends('layouts.administrator')

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Abounded Checkouts</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Abounded Checkouts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <div class="box-header with-border">
                    <div class="float-right">
                        <div class="btn-group pull-right" style="margin-right: 10px">
                            <div class="menu-right">
                                
                            </div>
                        </div>
                    </div>
                    <div class="float-left">
                        <div class="menu-left">

                        </div>

                        <div class="menu-left">
                        
                        </div>
                    </div>
                </div>

                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <strong> {{ Session::get('success') }} </strong>
                    </div>
                @endif

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Customer Email</th>
                    <th>Product ID</th>
                    <th>Image</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if($cartLists)
                    @foreach($cartLists as $cartList)
                      @if($cartList->product)
                        <tr>
                            <td> {{ $cartList->user->name }}</td>
                            <td> {{ $cartList->user->phone }}</td>
                            <td> {{ $cartList->user->email }}</td>
                            <td> P-{{ $cartList->product->id }}</td>
                            <td> <img src="{{ asset($cartList->product->images[0]->image) }}" width="100"> </td>
                            <td> {{ $cartList->product->sku }} </td>
                            <td> {{ $cartList->product->descriptions[0]->name }} </td>
                            <td> {{ $cartList->product->categories[0]->descriptions[0]->title }} </td>
                            <td> {{ $cartList->product->price }} </td>
                            <td> {{ $cartList->product->sale_price }} </td>
                        </tr>
                      @endif
                    @endforeach
                  @endif
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Customer Email</th>
                    <th>Product ID</th>
                    <th>Image</th>
                    <th>SKU</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  @endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
            });
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });
    </script>
@endsection
