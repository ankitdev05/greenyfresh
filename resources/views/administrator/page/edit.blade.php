@extends('layouts.administrator')

@section('style')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Page Edit</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('administrator.dashboard') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('administrator.page') }}">Page</a></li>
              <li class="breadcrumb-item active">Page Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong> {{ Session::get('success') }} </strong>
            </div>
        @endif

      <form action="{{ route('administrator.page_edit', $page->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-10">
            <div class="card card-secondary">
                <div class="card-header">
                <h3 class="card-title">Page</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <!-- <i class="fas fa-minus"></i>--></button>
                </div>
                </div>
                <div class="card-body">
                <div class="form-group">
                    <label for="inputTitle">Title</label>
                    <input type="text" name="title" value="{{ $page->descriptions[0]->title }}" id="inputTitle" class="form-control" max="200">
                    @if($errors->has('title'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('title') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputKeyword">Keyword</label>
                    <input type="text" name="keyword" value="{{ $page->descriptions[0]->keyword }}" id="inputKeyword" class="form-control" max="200">
                    @if($errors->has('keyword'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('keyword') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputDescription">Description</label>
                    <input type="text" name="description" value="{{ $page->descriptions[0]->description }}" id="inputDescription" class="form-control" max="300">
                    @if($errors->has('description'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('description') }} </strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputContent">Content</label>
                    <textarea name="content" id="inputContent" class="form-control">{{ $page->descriptions[0]->content }}</textarea>
                    @if($errors->has('content'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('content') }} </strong>
                        </div>
                    @endif
                </div>

                <BR>
                <HR>
                <HR>
                <BR>

                <div class="form-group">
                    <label for="inputType">Use In</label>
                    <select name="use_in" id="inputType" class="form-control">
                        <option value="">Select</option>
                        <option value="1" @if($page->use_in == 1) selected @endif>Header</option>
                        <option value="2" @if($page->use_in == 2) selected @endif>Footer</option>
                        <option value="3" @if($page->use_in == 3) selected @endif>Both</option>
                        <option value="4" @if($page->use_in == 4) selected @endif>APP</option>
                    </select>
                    @if($errors->has('use_in'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('use_in') }} </strong>
                        </div>
                    @endif
                </div>

                <div class="form-group clearfix">
                    <label for="inputStatus">Status</label>
                    <div class="icheck-success d-inline">
                        <input type="checkbox" @if($page->status == 1) checked="" @endif name="status" value="1" id="checkboxSuccess1" style="width: 20px;height: 20px;margin-left: 40px;">
                        <label for="checkboxSuccess1">
                        </label>
                    </div>
                    @if($errors->has('status'))
                        <div class="invalid-feedback" role="alert">
                            <strong> {{ $errors->first('status') }} </strong>
                        </div>
                    @endif
                </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-10">
            <a href="{{ route('administrator.page') }}" class="btn btn-secondary">Cancel</a>
            <input type="submit" value="Update" class="btn btn-success float-right">
            </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>

  @endsection

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
    $(function () {
        // Summernote
        $('#inputContent').summernote();
    })
    </script>
@endsection