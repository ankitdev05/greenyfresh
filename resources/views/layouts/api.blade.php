<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('website/img/favicon-32x32.png') }}" type="image/x-icon">  
    <title>Greeny | Apki apni sabji ki dukan</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('website/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('website/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('website/css/elegant-icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('website/css/nice-select.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('website/css/jquery-ui.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('website/css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('website/css/slicknav.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('website/css/style.css') }}" type="text/css">
    <style>
        .header__logo img{ width: 183px;height: 84px; }
        .footer{padding-top: 29px;}
        .footer__about {margin-bottom: 0px;}
        .login_popup input{margin-bottom: 10px!important;}
        .header__top__right__language a{color:#000!important;}
    </style>
    @yield('style')

</head>

<body>

    <!-- Categories Section Begin -->
    @yield('content')
    <!-- Latest Product Section End -->

    <!-- Js Plugins -->
    <script src="{{ asset('website/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('website/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('website/js/jquery.slicknav.js') }}"></script>
    <script src="{{ asset('website/js/mixitup.min.js') }}"></script>
    <script src="{{ asset('website/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('website/js/main.js') }}"></script>

    @yield('scripts')

</body>

</html>
