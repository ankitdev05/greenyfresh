<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    
    Route::get('/mydeletefile','AuthController@mydeletefile');
    
    Route::post('/register','AuthController@register');
    Route::post('/login','AuthController@login');
    Route::post('/checkuser','AuthController@checkuser');

    Route::get('/banner','ShopController@banner');

    Route::get('/advertise','ShopController@advertise');

    Route::get('/category','ShopController@category');

    Route::get('/products/{userid?}','ShopController@products');
    Route::get('/productsbycategory/{id}/{userid?}','ShopController@productsbycategory');
    Route::get('/productsbyid/{id}/{userid?}','ShopController@productsbyid');
    
    Route::post('/forgot_password', 'AuthController@forgotpassword');
    Route::post('/reset', 'AuthController@reset');

    Route::group(['middleware'=>'auth:api'], function() {

    	Route::get('/user_details', 'AuthController@details');
    	Route::post('/user_update', 'AuthController@update');
    	Route::post('/password_change', 'AuthController@updatepassword');
        Route::get('/logout', 'AuthController@logout');

        Route::get('/address', 'AuthController@addresslist');
        Route::post('/add_address', 'AuthController@addaddress');
        Route::get('/delete_address/{id}', 'AuthController@addressdelete');

        Route::get('/wishlist/{id}', 'AuthController@wishlist');
        Route::get('/wishlist_products', 'AuthController@wishlistProducts');

        //Add to cart
        Route::post('/addtocart', 'ShopController@addtocart');
        Route::get('/removecart/{id}', 'ShopController@removecart');
        Route::post('/updatecart', 'ShopController@updatecart');
        Route::get('/cart', 'ShopController@cart');

        Route::get('/cart_header', 'ShopController@cartheader');
        
        Route::get('/checkout', 'ShopController@checkout');
        Route::post('/place_order', 'ShopController@placeorder');

        Route::get('/orders', 'ShopController@orders');
        Route::get('/order_detail/{id}', 'ShopController@orderdetail');
    });

});
