<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('home');

Route::get('/login', 'HomeController@login')->name('login');
Route::post('/login', 'HomeController@login')->name('login');

Route::get('/register', 'HomeController@register')->name('register');
Route::post('/register', 'HomeController@register')->name('register');

Route::get('/profile', 'UserController@profile')->name('profile');
Route::post('/profile_update', 'UserController@update')->name('profile_update');
Route::get('/user_password', 'UserController@password')->name('user_password');
Route::post('/update_password', 'UserController@updatepassword')->name('update_password');

Route::get('/forgot_password', 'UserController@forgotpassword')->name('forgot_password');
Route::post('/forgot_password', 'UserController@forgotpassword')->name('forgot_password');
Route::get('/reset', 'UserController@reset')->name('reset');
Route::post('/reset', 'UserController@reset')->name('reset');

Route::get('/address', 'UserController@address')->name('address');
Route::get('/add_address', 'UserController@addAddress')->name('add_address');
Route::post('/get_address', 'UserController@getaddress')->name('get_address');
Route::post('/add_address', 'UserController@addAddress')->name('add_address');
Route::get('/delete_address/{id}', 'UserController@deleteAddress')->name('delete_address');

Route::get('/orders', 'UserController@orders')->name('orders');
Route::get('/orders/detail/{id?}', 'UserController@orderdetails')->name('orders/detail');

Route::get('/logout', 'UserController@logout')->name('logout');

Route::get('/contact', 'StaticController@contact')->name('contact');
Route::get('/about', 'StaticController@about')->name('about');
Route::get('/delivery_information', 'StaticController@delivery_infomation')->name('delivery_information');
Route::get('/privacy_policy', 'StaticController@privacy_policy')->name('privacy_policy');
Route::get('/terms_and_conditions', 'StaticController@terms_and_conditions')->name('terms_and_conditions');

Route::get('/api_about', 'StaticController@apiabout')->name('api_about');
Route::get('/api_privacy_policy', 'StaticController@apiprivacy_policy')->name('api_privacy_policy');
Route::get('/api_terms', 'StaticController@apiterms')->name('api_terms');

Route::get('/shop/{alias?}', 'HomeController@shop')->name('shop');
Route::get('/search', 'HomeController@searchproducts')->name('search');

Route::get('/shop/description/{alias?}', 'HomeController@productsbyid')->name('shop/description');

Route::post('/addtocart', 'UserController@addtocart')->name('addtocart');
Route::get('/directaddtocart/{alias}', 'UserController@directaddtocart')->name('directaddtocart');

Route::get('/wishlist/{alias}', 'UserController@wishlist')->name('wishlist');
Route::get('/wishlist_products', 'UserController@wishlistProducts')->name('wishlist_products');

Route::get('/remove_cart/{id}', 'UserController@removecart')->name('remove_cart');
Route::post('/update_cart', 'UserController@updatecart')->name('update_cart');

Route::get('/cart', 'UserController@cart')->name('cart');
Route::get('/checkout', 'UserController@checkout')->name('checkout');
Route::post('/place_order', 'UserController@placeorder')->name('place_order');

Route::get('/online_payment', 'UserController@onlinePlaceOrder')->name('online_payment');

Route::get('payment-online', 'PaymentController@create')->name('payment-online');

Route::post('payment', 'PaymentController@payment')->name('payment');

Route::post('/paypal_status', 'PaymentController@paymentCallback')->name('paypal_status');


