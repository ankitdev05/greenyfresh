<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'administrator'], function () {

    Route::get('/', 'AdminController@login')->name('login');
    Route::get('/login', 'AdminController@login')->name('login');
    Route::post('/login', 'AdminController@login')->name('login');

    Route::get('/register', 'AdminController@register')->name('register');
    Route::post('/register', 'AdminController@register')->name('register');

    Route::group(['middleware' => ['admin']], function() {

        Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');
        // Category
        Route::get('/category', 'CategoryController@index')->name('category');
        Route::get('/category_add', 'CategoryController@create')->name('category_add');
        Route::post('/category_add', 'CategoryController@create')->name('category_add');
        Route::get('/category_edit/{id}', 'CategoryController@update')->name('category_edit');
        Route::post('/category_edit/{id}', 'CategoryController@update')->name('category_edit');
        Route::get('/category_delete/{id}', 'CategoryController@delete')->name('category_delete');

        // Customer
        Route::get('/customer', 'CustomerController@index')->name('customer');

        // Product
        Route::get('/product', 'ProductController@index')->name('product');
        Route::get('/product_add', 'ProductController@create')->name('product_add');
        Route::post('/product_add', 'ProductController@create')->name('product_add');
        Route::get('/product_edit/{id}', 'ProductController@update')->name('product_edit');
        Route::post('/product_edit/{id}', 'ProductController@update')->name('product_edit');
        Route::get('/product_delete/{id}', 'ProductController@delete')->name('product_delete');

        // Tax
        Route::get('/tax', 'TaxController@index')->name('tax');
        Route::get('/tax_add', 'TaxController@create')->name('tax_add');
        Route::post('/tax_add', 'TaxController@create')->name('tax_add');
        Route::get('/tax_edit/{id}', 'TaxController@update')->name('tax_edit');
        Route::post('/tax_edit/{id}', 'TaxController@update')->name('tax_edit');
        Route::get('/tax_delete/{id}', 'TaxController@delete')->name('tax_delete');

        //Banner
        Route::get('/banner', 'BannerController@index')->name('banner');
        Route::get('/banner_add', 'BannerController@create')->name('banner_add');
        Route::post('/banner_add', 'BannerController@create')->name('banner_add');
        Route::get('/banner_edit/{id}', 'BannerController@update')->name('banner_edit');
        Route::post('/banner_edit/{id}', 'BannerController@update')->name('banner_edit');

        //Advertisement
        Route::get('/advertise', 'AdvertiseController@index')->name('advertise');
        Route::get('/advertise_add', 'AdvertiseController@create')->name('advertise_add');
        Route::post('/advertise_add', 'AdvertiseController@create')->name('advertise_add');
        Route::get('/advertise_edit/{id}', 'AdvertiseController@update')->name('advertise_edit');
        Route::post('/advertise_edit/{id}', 'AdvertiseController@update')->name('advertise_edit');
        Route::post('/advertise_delete/{id}', 'AdvertiseController@delete')->name('advertise_delete');

        //Pages
        Route::get('/page', 'PageController@index')->name('page');
        Route::get('/page_add', 'PageController@create')->name('page_add');
        Route::post('/page_add', 'PageController@create')->name('page_add');
        Route::get('/page_edit/{id}', 'PageController@update')->name('page_edit');
        Route::post('/page_edit/{id}', 'PageController@update')->name('page_edit');
        Route::post('/page_delete/{id}', 'PageController@delete')->name('page_delete');

        // Abounded Checkouts
        Route::get('/abounded', 'OrderController@abounded')->name('abounded');
        Route::get('/orders', 'OrderController@orders')->name('orders');
        Route::get('/order_detail/{id}', 'OrderController@orderdetail')->name('order_detail');
        Route::post('/order_status_change', 'OrderController@orderstatuschange')->name('order_status_change');

        //Coupons
        // Route::get('/coupon', 'CouponController@index')->name('coupon');
        // Route::get('/coupon_add', 'CouponController@create')->name('coupon_add');
        // Route::post('/coupon_add', 'CouponController@create')->name('coupon_add');
        // Route::get('/coupon_edit/{id}', 'CouponController@update')->name('coupon_edit');
        // Route::post('/coupon_edit/{id}', 'CouponController@update')->name('coupon_edit');
        // Route::post('/coupon_delete/{id}', 'CouponController@delete')->name('coupon_delete');

        // Attribute
        Route::get('/attribute', 'AttributeController@index')->name('attribute');
        Route::get('/attribute_add', 'AttributeController@create')->name('attribute_add');
        Route::post('/attribute_add', 'AttributeController@create')->name('attribute_add');
        Route::get('/attribute_edit/{id}', 'AttributeController@update')->name('attribute_edit');
        Route::post('/attribute_edit/{id}', 'AttributeController@update')->name('attribute_edit');

        Route::get('/attribute_option/{id}', 'AttributeController@add_attribute_option')->name('attribute_option');
        Route::get('/attribute_option_add/{id}', 'AttributeController@create_attribute_option')->name('attribute_option_add');
        Route::post('/attribute_option_add/{id}', 'AttributeController@create_attribute_option')->name('attribute_option_add');
        Route::get('/attribute_option_edit/{id}/{aid}', 'AttributeController@update_attribute_option')->name('attribute_option_edit');
        Route::post('/attribute_option_edit/{id}/{aid}', 'AttributeController@update_attribute_option')->name('attribute_option_edit');

        Route::post('/attribute_option_ajax', 'AttributeController@ajax_attribute_option')->name('attribute_option_ajax');
        

    });
});
