<?php
#app/Models/ShopUserAddress.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $guarded    = [];
    public $timestamps    = false;
    public $table = 'user_address';
}
