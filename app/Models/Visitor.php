<?php
#app/Models/ShopPage.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Visitor extends Model
{
    
    public $timestamps = false;
    public $table = 'visitors';
    protected $guarded = [];

}
