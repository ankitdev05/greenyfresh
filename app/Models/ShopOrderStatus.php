<?php
#app/Models/ShopOrderStatus.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopOrderStatus extends Model
{
    public $timestamps     = false;
    public $table = 'shop_order_status';
    protected $guarded           = [];
    
    
}
