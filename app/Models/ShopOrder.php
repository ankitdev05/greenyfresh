<?php
#app/Models/ShopOrder.php
namespace App\Models;

use App\Models\ShopOrderDetail;
use App\Models\ShopOrderTotal;
use App\Models\ShopProduct;
use DB;
use Illuminate\Database\Eloquent\Model;

class ShopOrder extends Model
{

    public $table = 'shop_orders';
    protected $guarded = [];


    public function details()
    {
        return $this->hasMany(ShopOrderDetail::class, 'order_id', 'id');
    }
    public function orderTotal()
    {
        return $this->hasMany(ShopOrderTotal::class, 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function orderStatus()
    {
        return $this->hasOne(ShopOrderStatus::class, 'id', 'status');
    }

/**
 * Add order detail
 * @param [type] $dataDetail [description]
 */
    public function addOrderDetail($dataDetail)
    {
        return ShopOrderDetail::create($dataDetail);
    }

    /**
     * Get list order new
     */
    public function getOrderNew() {
        $this->sc_status = 1;
        return $this;
    }

    /**
     * Get list order processing
     */
    public function getOrderProcessing() {
        $this->sc_status = 2;
        return $this;
    }

    /**
     * Get list order hold
     */
    public function getOrderHold() {
        $this->sc_status = 3;
        return $this;
    }

    /**
     * Get list order canceld
     */
    public function getOrderCanceled() {
        $this->sc_status = 4;
        return $this;
    }

    /**
     * Get list order done
     */
    public function getOrderDone() {
        $this->sc_status = 5;
        return $this;
    }

    /**
     * Get list order failed
     */
    public function getOrderFailed() {
        $this->sc_status = 6;
        return $this;
    }
    /**
     * Get Sum order total In Year
     *
     * @return  [type]  [return description]
     */
    public function getSumOrderTotalInYear() {
        return $this->selectRaw('DATE_FORMAT(created_at, "%Y-%m") AS ym,
        SUM(total/exchange_rate) AS total_amount')
        ->whereRaw('created_at >=  DATE_FORMAT(DATE_SUB(CURRENT_DATE(), INTERVAL 12 MONTH), "%Y-%m-00")')
        ->groupBy('ym')->get();
    }

    /**
     * Get Sum order total In month
     *
     * @return  [type]  [return description]
     */
    public function getSumOrderTotalInMonth() {
        return $this->selectRaw('DATE_FORMAT(created_at, "%m-%d") AS md,
        SUM(total/exchange_rate) AS total_amount, count(id) AS total_order')
        ->whereRaw('created_at >=  DATE_FORMAT(DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH), "%Y-%m-%d 00:00")')
        ->groupBy('md')->get();
    }

}
