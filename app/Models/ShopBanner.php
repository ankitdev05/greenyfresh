<?php
#app/Models/ShopBanner.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ShopBanner extends Model
{

    public $table = 'shop_banners';
    protected $guarded = [];
 
    /**
     * Get info detail
     *
     * @param   [int]  $id     
     * @param   [int]  $status 
     *
     */
    public function getDetail($id, $status = 1) {
        if ($status) {
            return $this->where('id', (int)$id)->where('status', 1)->first();
        } else {
            return $this->find((int)$id);
        }
    }


}
