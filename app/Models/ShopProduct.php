<?php
#app/Models/ShopProduct.php
namespace App\Models;

use App\Models\ShopCategory;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ShopTax;
use DB;
use Illuminate\Database\Eloquent\Model;
use Cache;

class ShopProduct extends Model
{
    public $table = 'shop_products';
    protected $guarded = [];

    public function categories() {
        return $this->belongsToMany(ShopCategory::class, ShopProductCategory::class, 'product_id', 'category_id');
    }
    public function images() {
        return $this->hasMany(ShopProductImage::class, 'product_id', 'id');
    }
    public function descriptions() {
        return $this->hasMany(ShopProductDescription::class, 'product_id', 'id');
    }
    public function prices() {
        return $this->hasMany(ShopProductPrice::class, 'product_id', 'id');
    }
    
    /**
     * Get product detail
     * @param  [string] $key [description]
     * @param  [string] $type id, sku, alias
     * @param  [''|int] $status
     * '' if is all status
     * @return [type]     [description]
     */
    public function getDetail($key = null, $type = null,  $status = 1)
    {
        if(empty($key)) {
            return null;
        }
        $tableDescription = (new ShopProductDescription)->getTable();
        $product = $this
            ->leftJoin($tableDescription, $tableDescription . '.product_id', $this->getTable() . '.id')
            ->where($tableDescription . '.lang', sc_get_locale());

        if(empty($type)) {
            $product = $product->where('id', (int)$key);
        } elseif ($type == 'alias') {
            $product = $product->where('alias', $key);
        } elseif ($type == 'sku') {
            $product = $product->where('sku', $key);
        } else {
            return null;
        }
        if($status) {
            $product = $product->where('status', (int)$status);
        }

        $product = $product
            ->with('images')
            ->with('promotionPrice');
        $product = $product->first();
        return $product;
    }

    /**
     * [getPercentDiscount description]
     * @return [type] [description]
     */
    public function getPercentDiscount()
    {
        //return round((($this->price - $this->getFinalPrice()) / $this->price) * 100);
    }

    /*
    Upate stock, sold
     */
    public static function updateStock($product_id, $qty_change)
    {
        $item = self::find($product_id);
        if ($item) {
            $item->stock = $item->stock - $qty_change;
            $item->sold = $item->sold + $qty_change;
            $item->save();
        }

    }
}
