<?php
#app/Models/ShopTax.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopAttribute extends Model
{
    public $timestamps = false;
    public $table = 'shop_attribute';
    protected $guarded = [];

}
