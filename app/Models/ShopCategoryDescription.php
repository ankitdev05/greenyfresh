<?php
#app/Models/ShopCategoryDescription.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopCategoryDescription extends Model
{
    public $timestamps    = false;
    public $table = 'shop_category_description';

    protected $guarded    = [];
}
