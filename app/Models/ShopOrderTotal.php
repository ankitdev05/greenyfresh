<?php
#app/Models/ShopOrderTotal.php
namespace App\Models;

use App\Models\ShopOrder;
use App\Models\ShopCurrency;
use Cart;
use Illuminate\Database\Eloquent\Model;

class ShopOrderTotal extends Model
{
    protected $table = 'shop_order_totals';
    protected $fillable = ['order_id', 'title', 'code', 'value', 'text', 'sort'];
    protected $guarded = [];
    
    /**
     * Update new sub total
     * @param  [int] $order_id [description]
     * @return [type]           [description]
     */
    public static function updateSubTotal($order_id)
    {
        try {
            $order = ShopOrder::find($order_id);
            $details = $order->details;
            $tax = $subTotal = 0;
            if($details->count()) {
                foreach ($details as $detail) {
                    $tax +=$detail->tax;
                    $subTotal +=$detail->total_price;
                }
            }
            $order->subtotal = $subTotal;
            $order->tax = $tax;
            $total = $subTotal + $tax + $order->discount + $order->shipping;
            $balance = $total + $order->received;
            $payment_status = 0;
            if ($balance == $total) {
                $payment_status = self::NOT_YET_PAY; //Not pay
            } elseif ($balance < 0) {
                $payment_status = self::NEED_REFUND; //Need refund
            } elseif ($balance == 0) {
                $payment_status = self::PAID; //Paid
            } else {
                $payment_status = self::PART_PAY; //Part pay
            }
            $order->payment_status = $payment_status;
            $order->total = $total;
            $order->balance = $balance;
            $order->save();

            //Update total
            $updateTotal = self::where('order_id', $order_id)
                ->where('code', 'total')
                ->first();
            $updateTotal->value = $total;
            $updateTotal->save();
            
            //Update Subtotal
            $updateSubTotal = self::where('order_id', $order_id)
                ->where('code', 'subtotal')
                ->first();
            $updateSubTotal->value = $subTotal;
            $updateSubTotal->save();

            //Update tax
            $updateSubTotal = self::where('order_id', $order_id)
            ->where('code', 'tax')
            ->first();
            $updateSubTotal->value = $tax;
            $updateSubTotal->save();

            return 1;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update data when row of total change
     * @param  [array] $row [description]
     * @return [void]
     */
    public static function updateRowTotal($row)
    {
        //Udate row
        $upField = self::find($row['id']);
        $upField->value = $row['value'];
        $upField->text = $row['text'];
        $upField->updated_at = date('Y-m-d H:i:s');
        $upField->save();
        $order_id = $upField->order_id;

        //Sum value item order total
        $totalData = self::where('order_id', $order_id)->get();
        $total = $discount = $shipping = $received = 0;
        foreach ($totalData as $key => $value) {
            if ($value['code'] === 'subtotal') {
                $total += $value['value'];
            }
            if ($value['code'] === 'tax') {
                $total += $value['value'];
            }
            if ($value['code'] === 'discount') {
                $discount += $value['value'];
                $total += $value['value'];
            }
            if ($value['code'] === 'shipping') {
                $shipping += $value['value'];
                $total += $value['value'];
            }
            if ($value['code'] === 'received') {
                $received += $value['value'];
            }
        }

        //Update total
        $updateTotal = self::where('order_id', $order_id)
            ->where('code', 'total')
            ->first();
        $updateTotal->value = $total;
        $updateTotal->save();

        //Update Order
        $order = ShopOrder::find($order_id);
        $order->discount = $discount;
        $order->shipping = $shipping;
        $order->received = $received;
        $order->balance = $total + $received;
        $order->total = $total;
        $order->save();
    }

}
