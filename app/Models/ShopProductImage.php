<?php
#app/Models/ShopProductImage.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopProductImage extends Model
{
    public $timestamps = false;
    public $table = 'shop_product_images';
    protected $guarded = [];
    
}
