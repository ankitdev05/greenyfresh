<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Authenticatable
{
    use Notifiable;
    Protected $gurad = 'administrator';
    Protected $fillable = [
        'name', 'email', 'password'
    ];
    Protected $hidden = [
        'password', 'remember_token'
    ];
}
