<?php
#app/Models/ShopTax.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopTax extends Model
{
    public $timestamps = false;
    public $table = 'shop_tax';
    protected $guarded = [];

}
