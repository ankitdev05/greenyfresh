<?php
#app/Models/ShopPage.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class TempOrder extends Model
{
    
    public $timestamps = false;
    public $table = 'temp_order';
    protected $guarded = [];

}
