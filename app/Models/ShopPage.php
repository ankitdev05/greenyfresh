<?php
#app/Models/ShopPage.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class ShopPage extends Model
{
    
    public $timestamps = false;
    public $table = 'shop_pages';
    protected $guarded = [];

    public function descriptions()
    {
        return $this->hasMany(ShopPageDescription::class, 'page_id', 'id');
    }
}
