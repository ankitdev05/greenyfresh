<?php
#app/Models/ShopCategory.php
namespace App\Models;

use App\Models\ShopCategoryDescription;
use App\Models\ShopProduct;
use Cache;
use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model
{

    public $table = 'shop_category';
    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany(ShopProduct::class, 'shop_product_category', 'category_id', 'product_id');
    }

    public function descriptions()
    {
        return $this->hasMany(ShopCategoryDescription::class, 'category_id', 'id');
    }

}
