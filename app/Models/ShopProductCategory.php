<?php
#app/Models/ShopProductCategory.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopProductCategory extends Model
{
    protected $guarded    = [];
    public $timestamps    = false;
    public $table = 'shop_product_category';
}
