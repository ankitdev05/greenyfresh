<?php
#app/Models/ShopTax.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopAttributeOption extends Model
{
    public $timestamps = false;
    public $table = 'shop_attribute_options';
    protected $guarded = [];

}
