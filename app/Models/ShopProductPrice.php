<?php
#app/Models/ShopProduct.php
namespace App\Models;

use App\Models\ShopCategory;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ShopTax;
use DB;
use Illuminate\Database\Eloquent\Model;
use Cache;

class ShopProductPrice extends Model
{
    public $table = 'shop_product_prices';
    protected $guarded = [];


}
