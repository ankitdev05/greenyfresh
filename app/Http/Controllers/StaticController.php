<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\ShopPage;
use App\Models\ShopPageDescription;
use Auth;
use Session;
use GuzzleHttp\Client;

class StaticController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
    */

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function contact() {
        return view('web.contact');
    }

    public function about() {
        $alias = "about";
        $page = ShopPage::with('descriptions')->where('alias', $alias)->first();
        return view('web.common', compact('page'));
    }

    public function show() {
        $alias = "about";
        $page = ShopPage::with('descriptions')->where('alias', $alias)->first();
        return view('web.common', compact('page'));
    }

    public function delivery_infomation() {
        $alias = "delivery_information";
        $page = ShopPage::with('descriptions')->where('alias', $alias)->first();
        return view('web.common', compact('page'));
    }

    public function privacy_policy() {
        $alias = "privacy_policy";
        $page = ShopPage::with('descriptions')->where('alias', $alias)->first();
        return view('web.common', compact('page'));
    }
    
    public function terms_and_conditions() {
        $alias = "terms_and_conditions";
        $page = ShopPage::with('descriptions')->where('alias', $alias)->first();
        return view('web.common', compact('page'));
    }

    


    public function apiabout() {
        $alias = "api_about";
        $page = ShopPage::with('descriptions')->where('alias', $alias)->first();
        return view('web.apicommon', compact('page'));
    }

    public function apiprivacy_policy() {
        $alias = "api_privacy_policy";
        $page = ShopPage::with('descriptions')->where('alias', $alias)->first();
        return view('web.apicommon', compact('page'));
    }

    public function apiterms() {
        $alias = "api_terms_and_conditions";
        $page = ShopPage::with('descriptions')->where('alias', $alias)->first();
        return view('web.apicommon', compact('page'));
    }

}
