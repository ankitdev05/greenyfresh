<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopAttribute;
use App\Models\ShopAttributeOption;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class AttributeController extends Controller
{
    public function index(Request $request) {
        $attributeLists = ShopAttribute::all();

        return view('administrator.attribute.list', compact('attributeLists'));
    }

    public function create(Request $request) {

        if($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
            ]);

            ShopAttribute::create([
                "name" => $request->name,
            ]);

            Session::flash('success', 'Added Successfully');
            return view('administrator.attribute.create');
        }
        return view('administrator.attribute.create');
    }

    public function delete($id) {
        ShopAttribute::where('id', $id)->delete();

        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('administrator.attribute');
    }

    public function update(Request $request, $id) {
        $attribute = ShopAttribute::where('id', $id)->first();

        if($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required',
            ]);

            $attribute->name = $request->name;
            $attribute->save();

            Session::flash('success', 'Updated Successfully');
            return redirect()->route('administrator.attribute');
        }

        return view('administrator.attribute.edit', compact('attribute'));
    }

    public function add_attribute_option(Request $request,$id) {
        $attributeOptionLists = ShopAttributeOption::where(["attribute_id"=>$id])->get();

        return view('administrator.attribute.option_list', compact('attributeOptionLists', 'id'));
    }

    public function create_attribute_option(Request $request, $id) {

        if($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
            ]);

            ShopAttributeOption::create([
                'attribute_id'=>$id,
                "option_name" => $request->name,
            ]);

            Session::flash('success', 'Added Successfully');
            return view('administrator.attribute.option_create', compact('id'));
        }
        return view('administrator.attribute.option_create', compact('id'));
    }

    public function update_attribute_option(Request $request, $id, $aid) {
        $attribute = ShopAttributeOption::where('id', $aid)->first();

        if($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required',
            ]);

            $attribute->option_name = $request->name;
            $attribute->save();

            Session::flash('success', 'Updated Successfully');
            return redirect()->back();
        }

        return view('administrator.attribute.option_edit', compact('attribute','id'));
    }

    public function ajax_attribute_option(Request $request) {

        $attributeOptionLists = ShopAttributeOption::where(["attribute_id"=>$request->selection])->get();
        $optionHTML = "";
        if($attributeOptionLists) {
            foreach($attributeOptionLists as $attributeOptionList) {
                $optionHTML .= "<option value='".$attributeOptionList->option_name."'>".$attributeOptionList->option_name."</option>";
            }
        }
        
        echo json_encode($optionHTML);
        exit;
    }
}
