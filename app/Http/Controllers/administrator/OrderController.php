<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopCategory;
use App\Models\ShopCategoryDescription;

use App\User;

use App\Models\ShopProduct;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ShopTax;

use App\Models\ShopCart;
use App\Models\ShopOrder;
use App\Models\ShopOrderDetail;

use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class OrderController extends Controller
{
    public function abounded(Request $request) {

        $cartLists = ShopCart::orderBy('id','DESC')->get();
        foreach($cartLists as $cartList) {
            
            $checkproduct = ShopProduct::where('id', $cartList->product_id)->first();
        	if($checkproduct) {
        	    $cartList->product = ShopProduct::with('descriptions')->with('images')->where('id', $cartList->product_id)->first();
        	    $cartList->user = User::where('id', $cartList->user_id)->first();
        	}
        }

        return view('administrator.order.abounded', compact('cartLists'));
    }

    public function orders(Request $request) {

        $orderLists = ShopOrder::with('details')->orderBy('id','DESC')->get();
        foreach($orderLists as $orderList) {
        	$orderList->user = User::where('id', $orderList->user_id)->first();
        }
        
        return view('administrator.order.order_list', compact('orderLists'));
    }

    public function orderdetail(Request $request, $id) {

        $orderList = ShopOrder::with('details')->where('id', $id)->first();
        $orderList->user = User::where('id', $orderList->user_id)->first();
        
        foreach($orderList->details as $orderdetail) {
            $product = ShopProductImage::where('product_id', $orderdetail->product_id)->first();
            if($product) {
                $orderdetail->image = $product->image; 
            } else {
                $orderdetail->image = "";
            }
        }
        
        return view('administrator.order.order_detail', compact('orderList'));
    }

    public function orderstatuschange(Request $request) {
        $order = ShopOrder::where('order_number', $request->id)->first();
        if($order) {

            $order->status = $request->status;
            $order->save();
            
            echo "true";
            exit;
        } else {
            echo "true";
            exit;
        }
    }

}