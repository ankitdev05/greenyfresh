<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ShopPage;
use App\Models\ShopPageDescription;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class PageController extends Controller
{
    public function index(Request $request) {
        $pageLists = ShopPage::with('descriptions')->get();
        return view('administrator.page.list', compact('pageLists'));
    }

    public function create(Request $request) {
        
        if($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required',
                'keyword' => 'required',
                'description' => 'required',
                'use_in' => 'required',
                'content' => 'required',
                'status' => 'required',
            ]);

            $page = ShopPage::create([
                "use_in" => $request->use_in,
                "status" => $request->status,
                "alias" => strtolower(str_replace(' ', '_', trim($request->title))),
            ]);
            
            ShopPageDescription::create([
                "page_id" => $page->id,
                "title" => $request->title,
                "keyword" => $request->keyword,
                "description" => $request->description,
                "content" => $request->content,
            ]);
            
            Session::flash('success', 'Added Successfully');
            return redirect()->route('administrator.page_add');
        }
        return view('administrator.page.create');
    }

    public function delete($id) {
        $page = ShopPage::where('id', $id)->delete();
        $page->descriptions()->delete();
        
        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('administrator.page');
    }

    public function update(Request $request, $id) {
        $page = ShopPage::with('descriptions')->where('id', $id)->first();
        
        if($request->isMethod('post')) {
            
            $this->validate($request, [
                'title' => 'required',
                'keyword' => 'required',
                'description' => 'required',
                'use_in' => 'required',
                'content' => 'required',
                'status' => 'required',
            ]);

            $page->use_in = $request->use_in;
            $page->alias = strtolower(str_replace(' ', '_', trim($request->title)));
            $page->status = $request->status;
            $page->save();

            $desc = ShopPageDescription::where('page_id',$page->id)->first();
            $desc->title = $request->title;
            $desc->keyword = $request->keyword;
            $desc->description = $request->description;
            $desc->content = $request->content;
            $desc->save();
            

            Session::flash('success', 'Updated Successfully');
            return redirect()->route('administrator.page');
        }

        return view('administrator.page.edit', compact('page'));
    }
}
