<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopTax;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class TaxController extends Controller
{
    public function index(Request $request) {
        $taxLists = ShopTax::all();

        return view('administrator.tax.list', compact('taxLists'));
    }

    public function create(Request $request) {

        if($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'value' => 'required',
            ]);

            ShopTax::create([
                "name" => $request->name,
                "value" => $request->value,
            ]);

            Session::flash('success', 'Added Successfully');
            return view('administrator.tax.create');
        }
        return view('administrator.tax.create');
    }

    public function delete($id) {
        ShopTax::where('id', $id)->delete();

        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('administrator.tax');
    }

    public function update(Request $request, $id) {
        $tax = ShopTax::where('id', $id)->first();

        if($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required',
                'value' => 'required',
            ]);

            $tax->name = $request->name;
            $tax->value = $request->value;
            $tax->save();

            Session::flash('success', 'Updated Successfully');
            return redirect()->route('administrator.tax');
        }

        return view('administrator.tax.edit', compact('tax'));
    }
}
