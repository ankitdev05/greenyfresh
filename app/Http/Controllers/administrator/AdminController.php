<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Model\Administrator;
use App\Models\ShopOrder;
use App\Models\ShopProduct;
use App\Models\Visitor;
use App\User;

use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class AdminController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request) {

        if($request->isMethod('post')) {

            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required|min:5'
            ]);

            if(Auth::guard('administrator')->attempt(['email' => $request->email, 'password' =>$request->password], $request->remember)) {
                return redirect()->route('administrator.dashboard');

            } else {
                Session::flash('error', 'Login Failed.');
                return view('administrator.login');
            }
        }
        return view('administrator.login');
    }

    public function register(Request $request) {

        if($request->isMethod('post')) {
            return view('administrator.login');
        }
        return view('administrator.register');
    }

    public function dashboard(Request $request) {
        $newOrder = ShopOrder::where(["status"=>0])->count();
        $deliveredOrder = ShopOrder::where(["status"=>3])->count();
        $activeProduct = ShopProduct::where(["status"=>1])->count();
        $activeUser = User::where(["status"=>1])->count();

        $orderLists = ShopOrder::with('details')->where(["status"=>0])->orderBy('id','DESC')->limit(10)->get();
        $productLists =ShopProduct::with('descriptions')->with('images')->with('categories')->orderBy('id','DESC')->limit(5)->get();
        
        $visitor = Visitor::all();
        if($visitor) {
            $countRecent = count($visitor);
        } else {
            $countRecent = 0;
        }
        
        return view('administrator.dashboard', compact('newOrder','deliveredOrder','activeProduct','orderLists','productLists','activeUser','countRecent'));
    }

}
