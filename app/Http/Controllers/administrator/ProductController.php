<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopCategory;
use App\Models\ShopCategoryDescription;

use App\Models\ShopProduct;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ShopProductPrice;
use App\Models\ShopTax;
use App\Models\ShopAttribute;
use App\Models\ShopAttributeOption;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class ProductController extends Controller
{
    public function index(Request $request) {
        $productLists = ShopProduct::with('descriptions')->with('images')->with('categories')->orderBy('id','DESC')->get();
        return view('administrator.product.list', compact('productLists'));
    }

    public function create(Request $request) {
        $taxData = ShopTax::all();
        $catData = ShopCategory::with('descriptions')->get();
        $attributeData = ShopAttribute::all();

        if($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'keyword' => 'required',
                'description' => 'required',
                'category' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg',
                'type' => 'required',
                'tax' => 'required',
                'sku' => 'required',
                'price' => 'required',
                'sale_price' => 'required',
                'stock' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('product_images'), $imageName);
                $imageNameSave = 'product_images/' . $imageName;
            }

            $pro = ShopProduct::create([
                "attribute_id" => $request->attribute,
                "option_name" => $request->option,
                "sku" => $request->sku,
                "price" => $request->price,
                "sale_price" => $request->sale_price,
                "stock" => $request->stock,
                "tax_id" => $request->tax,
                "type" => $request->type,
                "status" => $request->status??2,
                "alias" => strtolower(str_replace(' ', '_', trim($request->name))),
            ]);

            ShopProductCategory::create([
                "product_id" => $pro->id,
                "category_id" => $request->category,
            ]);
            if(strlen($request->option) > 0) {
                $proName = $request->name;
            } else {
                $proName = $request->name;
            }

            ShopProductDescription::create([
                "product_id" => $pro->id,
                "name" => $proName,
                "keyword" => $request->keyword,
                "description" => $request->description,
                "content" => $request->content,
            ]);
            ShopProductImage::create([
                "product_id" => $pro->id,
                "image" => $imageNameSave,
            ]);
            Session::flash('success', 'Added Successfully');
            return redirect()->route('administrator.product_add');
        }
        return view('administrator.product.create', compact('taxData', 'catData', 'attributeData'));
    }

    public function delete($id) {
        ShopProduct::where('id', $id)->delete();
        ShopProductDescription::where('product_id', $id)->delete();
        ShopProductImage::where('product_id', $id)->delete();
        ShopProductCategory::where('product_id', $id)->delete();

        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('administrator.product');
    }

    public function update(Request $request, $id) {
        $product = ShopProduct::with('descriptions')->with('images')->with('categories')->where('id', $id)->first();
        $taxData = ShopTax::all();
        $catData = ShopCategory::with('descriptions')->get();
        $attributeData = ShopAttribute::all();

        if($product->option_name) {
            $optionData = ShopAttributeOption::where('attribute_id', $product->attribute_id)->get();
        } else {
            $optionData = [];
        }

        if($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'keyword' => 'required',
                'description' => 'required',
                'category' => 'required',
                'image' => 'mimes:jpeg,png,jpg',
                'type' => 'required',
                'tax' => 'required',
                'sku' => 'required',
                'price' => 'required',
                'sale_price' => 'required',
                'stock' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('product_images'), $imageName);
                $imageNameSave = 'product_images/' . $imageName;

                $product->images[0]->image = $imageNameSave;
            }

            $product->attribute_id = $request->attribute;
            $product->option_name = $request->option;
            $product->sku = $request->sku;
            $product->price = $request->price;
            $product->sale_price = $request->sale_price;
            $product->stock = $request->stock;
            $product->tax_id = $request->tax;
            $product->type = $request->type;
            $product->alias = strtolower(str_replace(' ', '_', trim($request->name)));
            $product->status = $request->status??2;
            $product->save();

            $description = ShopProductDescription::where('product_id', $product->id)->first();
            if(strlen($request->option) > 0) {
                $description->name = $request->name;
            } else {
                $description->name = $request->name;
            }
            $description->keyword = $request->keyword;
            $description->description = $request->description;
            $description->content = $request->content;
            $description->save();

            $categories = ShopProductCategory::where('product_id', $product->id)->first();
            $categories->category_id = $request->category;
            $categories->save();

            Session::flash('success', 'Updated Successfully');
            return redirect()->route('administrator.product');
        }

        return view('administrator.product.edit', compact('product','catData','taxData','attributeData','optionData'));
    }
}
