<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopBanner;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class BannerController extends Controller
{
    public function index(Request $request) {
        $bannerLists = ShopBanner::all();

        return view('administrator.banner.list', compact('bannerLists'));
    }

    public function create(Request $request) {

        if($request->isMethod('post')) {

            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg',
                'url' => 'required',
                'target' => 'required',
                'sort' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('banner_images'), $imageName);
                $imageNameSave = 'banner_images/' . $imageName;
            }

            ShopBanner::create([
                "image" => $imageNameSave,
                "url" => $request->url,
                "target" => $request->target,
                "status" => 1,
                "sort" => $request->sort
            ]);

            Session::flash('success', 'Added Successfully');
            return view('administrator.banner.create');
        }
        return view('administrator.banner.create');
    }

    // public function delete($id) {
    //     $category = ShopCategory::where('id', $id)->delete();
    //     $category->descriptions()->delete();

    //     Session::flash('success', 'Deleted Successfully');
    //     return redirect()->route('administrator.category');
    // }

    public function update(Request $request, $id) {
        $banner = ShopBanner::where('id', $id)->first();

        if($request->isMethod('post')) {
            $this->validate($request, [
                'image' => 'mimes:jpeg,png,jpg',
                'url' => 'required',
                'target' => 'required',
                'sort' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('banner_images'), $imageName);
                $imageNameSave = 'banner_images/' . $imageName;
                $banner->image = $imageNameSave;
            }

            $banner->url = $request->url;
            $banner->target = $request->target;
            $banner->sort = $request->sort;
            $banner->save();

            Session::flash('success', 'Updated Successfully');
            return redirect()->route('administrator.banner');
        }

        return view('administrator.banner.edit', compact('banner'));
    }
}
