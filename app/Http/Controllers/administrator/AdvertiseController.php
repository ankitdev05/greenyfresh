<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopAdvertise;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class AdvertiseController extends Controller
{
    public function index(Request $request) {
        $advertiseLists = ShopAdvertise::all();

        return view('administrator.advertise.list', compact('advertiseLists'));
    }

    public function create(Request $request) {

        if($request->isMethod('post')) {

            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg',
                'url' => 'required',
                'sort' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('advertise_images'), $imageName);
                $imageNameSave = 'advertise_images/' . $imageName;
            }

            ShopAdvertise::create([
                "image" => $imageNameSave,
                "url" => $request->url,
                "status" => 1,
                "sort" => $request->sort
            ]);

            Session::flash('success', 'Added Successfully');
            return view('administrator.advertise.create');
        }
        return view('administrator.advertise.create');
    }

    public function delete($id) {
        ShopAdvertise::where('id', $id)->delete();
        
        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('administrator.advertise');
    }

    public function update(Request $request, $id) {
        $advertise = ShopAdvertise::where('id', $id)->first();

        if($request->isMethod('post')) {
            $this->validate($request, [
                'image' => 'mimes:jpeg,png,jpg',
                'url' => 'required',
                'sort' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('advertise_images'), $imageName);
                $imageNameSave = 'advertise_images/' . $imageName;
                $advertise->image = $imageNameSave;
            }

            $advertise->url = $request->url;
            $advertise->sort = $request->sort;
            $advertise->save();

            Session::flash('success', 'Updated Successfully');
            return redirect()->route('administrator.advertise');
        }

        return view('administrator.advertise.edit', compact('advertise'));
    }
}
