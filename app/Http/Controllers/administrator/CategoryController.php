<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopCategory;
use App\Models\ShopCategoryDescription;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class CategoryController extends Controller
{
    public function index(Request $request) {
        $categoryLists = ShopCategory::with('descriptions')->get();

        return view('administrator.category.list', compact('categoryLists'));
    }

    public function create(Request $request) {

        if($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required',
                'keyword' => 'required',
                'description' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg',
                'sort' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('category_images'), $imageName);
                $imageNameSave = 'category_images/' . $imageName;
            }

            $cat = ShopCategory::create([
                "image" => $imageNameSave,
                "alias" => strtolower(str_replace(' ', '_', trim($request->title))),
                "parent" => 0,
                "status" => 1,
                "sort" => $request->sort
            ]);
            ShopCategoryDescription::create([
                "category_id" => $cat->id,
                "title" => $request->title,
                "keyword" => $request->keyword,
                "description" => $request->description
            ]);
            Session::flash('success', 'Added Successfully');
            return view('administrator.category.create');
        }
        return view('administrator.category.create');
    }

    public function delete($id) {
        $category = ShopCategory::where('id', $id)->delete();
        $category->descriptions()->delete();

        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('administrator.category');
    }

    public function update(Request $request, $id) {
        $category = ShopCategory::with('descriptions')->where('id', $id)->first();

        if($request->isMethod('post')) {
            $this->validate($request, [
                'title' => 'required',
                'keyword' => 'required',
                'description' => 'required',
                'image' => 'mimes:jpeg,png,jpg',
                'sort' => 'required',
            ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('category_images'), $imageName);
                $imageNameSave = 'category_images/' . $imageName;

                $category->image = $imageNameSave;
            }

            $category->alias = strtolower(str_replace(' ', '_', trim($request->title)));
            $category->status = 1;
            $category->sort = $request->sort;

            $category->descriptions[0]->title = $request->title;
            $category->descriptions[0]->keyword = $request->keyword;
            $category->descriptions[0]->description = $request->description;
            $category->save();

            Session::flash('success', 'Updated Successfully');
            return redirect()->route('administrator.category');
        }

        return view('administrator.category.edit', compact('category'));
    }
}
