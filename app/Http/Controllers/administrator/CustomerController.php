<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class CustomerController extends Controller
{
    public function index(Request $request) {
        $userLists = User::orderBy('id', 'DESC')->get();

        return view('administrator.customer.list', compact('userLists'));
    }
}
