<?php

namespace App\Http\Controllers\administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopCoupon;
use Carbon\Carbon;
use Auth;
use Session;
use GuzzleHttp\Client;

class CouponController extends Controller
{
    public function index(Request $request) {
        $couponLists = ShopCoupon::all();

        return view('administrator.coupon.list', compact('couponLists'));
    }

    public function create(Request $request) {

        if($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'value' => 'required',
            ]);

            ShopCoupon::create([
                "name" => $request->name,
                "value" => $request->value,
            ]);

            Session::flash('success', 'Added Successfully');
            return view('administrator.coupon.create');
        }
        return view('administrator.coupon.create');
    }

    public function delete($id) {
        ShopCoupon::where('id', $id)->delete();

        Session::flash('success', 'Deleted Successfully');
        return redirect()->route('administrator.coupon');
    }

    public function update(Request $request, $id) {
        $coupon = ShopCoupon::where('id', $id)->first();

        if($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required',
                'value' => 'required',
            ]);

            $coupon->name = $request->name;
            $coupon->value = $request->value;
            $coupon->save();

            Session::flash('success', 'Updated Successfully');
            return redirect()->route('administrator.coupon');
        }

        return view('administrator.coupon.edit', compact('coupon'));
    }
}
