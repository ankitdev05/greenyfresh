<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;
use Lcobucci\JWT\Parser;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Models\UserAddress;
use App\Models\ShopWishlist;
use App\Models\ShopProduct;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ForgotPassword;
use Illuminate\Filesystem\Filesystem;

use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReset;

class AuthController extends Controller
{
    public function mydeletefile() {
        
        $folder = 'myfol';
 
        //Get a list of all of the file names in the folder.
        $files = glob($folder . '/*');
        var_dump($files);die;
        //Loop through the file list.
        foreach($files as $file){
            //Make sure that this is a file and not a directory.
            if(is_file($file)){
                //Use the unlink function to delete the file.
                unlink($file);
            } else {
                echo "No Files";exit;
            }
        }
    }
    
    public function checkuser(Request $request) {
        try {
            $phone = $request->phone;
            $flag1 = 'false';

            $checkphone = User::where(["phone"=>$request->phone])->first();
            if($checkphone) {
                $flag1 = 'true';
            } else {
                $flag1 = 'false';
            }

            if($flag1 == 'false') {
                return response()->json(['status'=>'true','message'=>"Not found"],Response::HTTP_OK);
            } else {
                return response()->json(['status'=>'true','message'=>"Already exists"],Response::HTTP_OK);
            }

        } catch (\Exception $e) {
            return response()->json(['status'=>'false','message'=>$e->getMessage()],Response::HTTP_LENGTH_REQUIRED);
        }
    }

    public function register(Request $request) {
        try {
            $validator= Validator::make($request->all(),[
                'full_name'=>'required',
                'email'=>'required|email|unique:users,email',
                'phone'=>'required|min:8|unique:users,phone',
                'country_code'=>['required','numeric'],
                'password'=>'required|max:20',
                'address'=>'required',
                'gender'=>'required',
                'dob'=>'required',
                'device_type'=>'required',
                'device_token'=>'required',
            ]);

            if($validator->fails()){
                return response()->json([
                'message' => $validator->errors()->first(),
                'status'=>'false',
                "registerResponse"=>(object)['message' => $validator->errors()->first(),
                'status'=>'false',]
                ],Response::HTTP_LENGTH_REQUIRED);
            }

            $user = new User;
            $user->email = $request->email;
            $user->name = $request->full_name;
            $user->address = $request->address;
            $user->phone = $request->phone;
            $user->country_code=$request->country_code;
            $user->gender = $request->gender;
            $user->dob = $request->dob;
            $user->device_type=$request->device_type;
            $user->device_token=$request->device_token;
            $user->is_phone_verify ='1';
            $user->password = bcrypt($request->password);
            $user->referal_code = $this->randomordernumber();
            $user->refered_code = $request->refered_code;
            $user->save();

            // $http = new Client;
            // $response = $http->post(url('oauth/token'), [
            //     'form_params' => [
            //     'grant_type' => 'password',
            //     'client_id' => '2',
            //     'client_secret' =>env('PASSPORT_CLIENT_SECRET',null),
            //     'username' => $request->email,
            //     'password' => $request->password,
            //     'scope' => '',
            //     ],
            // ]);

            $token = $user->createToken('academy')->accessToken;

            return response()->json(["registerResponse"=>['auth' => $token, 'user' => new UserResource($user)],'status'=>'true'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['status'=>'false','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function login(Request $request) {

        try {
            $validator= Validator::make($request->all(),[
                'phone_no' =>"required",
                'password'=>'required',
                'device_type'=>'required',
                'device_token'=>'required',
            ]);

            $user = User::where('phone',$request->phone_no)->first();

            if($validator->fails()){
                return response()->json([
                'message' => $validator->errors()->first(),
                'status'=>'false',
                "loginResponse"=>(object)[]
                ],Response::HTTP_OK);
            }

            if(!$user){
                return response(['status'=>'false','message'=>'User not found',"loginResponse"=>(object)[]]);
            }

            if($user->status !=1){
                return response(['status'=>'error','message'=>'User is block']);
            }
            if($user->is_phone_verify ==0){
                return response(['status'=>'error','message'=>'Phone no not verify.']);
            }

            $credentials = [
                "email" => $user->email,
                "password" => $request->password
            ];

            if(auth()->attempt($credentials)) {
                $user->device_type = $request->device_type;
                $user->device_token = $request->device_token;
                $user->save();

                $token = auth()->user()->createToken('academy')->accessToken;

                return response()->json(["loginResponse"=>['auth' => $token, 'user' => new UserResource($user)], 'status'=>'true'],Response::HTTP_OK);

            } else {
                return response()->json(['message'=>'password not match','status'=>'false'],Response::HTTP_OK);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>'false','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function logout(Request $request){
        try {
            $user = auth()->user()->token();
            $user->revoke();

            return response()->json(['message'=>"successful",'status'=>'true'], Response::HTTP_OK);
        } catch (\Exception $e) {

            return response()->json(['status'=>'false','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function details() {
        return response()->json(["details"=>['user' => auth()->user()], 'status'=>'true'],Response::HTTP_OK);
    }

    public function update(Request $request) {
        try {
            $validator= Validator::make($request->all(),[
                'full_name'=>'required',
                'phone'=>'required|min:8',
                'country_code'=>['required','numeric'],
                'address'=>'required',
                'gender'=>'required',
                'dob'=>'required',
            ]);

            if($validator->fails()){
                return response()->json([
                'message' => $validator->errors()->first(),
                'status'=>'false',
                "registerResponse"=>(object)[]
                ],Response::HTTP_LENGTH_REQUIRED);
            }

            $user = User::find(auth()->user()->id);
            //$user->email = $request->email;
            $user->name = $request->full_name;
            $user->address = $request->address;
            $user->phone = $request->phone;
            $user->country_code=$request->country_code;
            $user->gender = $request->gender;
            $user->dob = $request->dob;
            $user->save();

            return response()->json([
                    'status'=>'true',
                    'message'=>"User updated successfully"
                ],Response::HTTP_OK);

        } catch (\Exception $e) {
            return response()->json(['status'=>'false','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updatepassword(Request $request) {
        try {
            $validator= Validator::make($request->all(),[
                'old_password'=>'required',
                'new_password'=>'required',
            ]);

            if($validator->fails()){
                return response()->json([
                'message' => $validator->errors()->first(),
                'status'=>'false',
                "registerResponse"=>(object)[]
                ],Response::HTTP_LENGTH_REQUIRED);
            }

            if(!Hash::check(request()->old_password, auth()->user()->password)){
                return response()->json([
                    'status'=>'false',
                    'message'=>'Old password does not match',
                ],Response::HTTP_OK);
            }

            $user = User::find(auth()->user()->id);
            $user->password = bcrypt($request->new_password);
            $user->save();

            return response()->json([
                    'status'=>'true',
                    'message'=>"User password updated successfully"
                ],Response::HTTP_OK);

        } catch (\Exception $e) {
            return response()->json(['status'=>'false','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function forgotpassword(Request $request) {
        
        $validator= Validator::make($request->all(),[
            'email'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
            'message' => $validator->errors()->first(),
            'status'=>'false',
            "forgotpassword"=>(object)[]
            ],Response::HTTP_LENGTH_REQUIRED);
        }
        
        $user = User::where('email', $request->email)->first();
        if($user) {
            Mail::to(request()->email)->send(new PasswordReset(request()->email));
            
            return response()->json([
                'status'=>'true',
                'message'=>"Password change verification link has been sent to your email id"
            ],Response::HTTP_OK);
        } else {
            
            return response()->json(['status'=>'false','message'=>"Email id does not exists"],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    public function reset(Request $request) {
            
        $validator= Validator::make($request->all(),[
            'verify_code'=>'required',
            'email'=>'required',
            'new_password'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
            'message' => $validator->errors()->first(),
            'status'=>'false',
            "forgotpassword"=>(object)[]
            ],Response::HTTP_LENGTH_REQUIRED);
        }

        $verify = ForgotPassword::where(["email"=>$request->email, "code"=>$request->verify_code, "status"=>1])->first();
        if($verify) {
            
            $user = User::where('email', $request->email)->first();
            $user->password = bcrypt($request->new_password);
            $user->save();
            
            $verify->status = 2;
            $verify->save();

            return response()->json([
                'status'=>'true',
                'message'=>"Password Changed Successfully"
            ],Response::HTTP_OK);
            
        } else {
            return response()->json(['status'=>'false','message'=>"Email id or verification code does not match"],Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function addresslist(Request $request) {

        $address = UserAddress::where('user_id',auth()->user()->id)->get();
        return response()->json(["address"=>$address, 'status'=>'true'],Response::HTTP_OK);
    }

    public function addaddress(Request $request) {
        $validator = Validator::make(request()->all(),[
            'full_name'=>'required',
            'postcode'=>'required',
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'phone'=>'required',
            'landmark'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
            'message' => $validator->errors()->first(),
            'status'=>'false',
            "addAddress"=>(object)[]
            ],Response::HTTP_LENGTH_REQUIRED);
        }

        UserAddress::create([
            "user_id" => auth()->user()->id,
            "full_name"=>$request->full_name,
            "address"=>$request->address,
            "city"=>$request->city,
            "state"=>$request->state,
            "phone"=>$request->phone,
            "landmark"=>$request->landmark,
            "postcode"=>$request->postcode,
        ]);
        
        return response()->json(['address' => "Address added", 'status'=>'true'],Response::HTTP_OK);
    }

    public function addressdelete(Request $request, $id) {

        UserAddress::where('id', $id)->delete();
        return response()->json(["message"=>"Deleted Successfully", 'status'=>'true'],Response::HTTP_OK);
    }

    public function wishlist(Request $request, $id) {
        $id = $id;
        $product = ShopProduct::where('id', $id)->first();
        $wish = ShopWishlist::where(["user_id"=>auth()->user()->id, "product_id"=>$product->id])->first();
        if($wish) {

            $wish->delete();

            return response()->json(['wishlist' => "Wishlist removed", 'status'=>'true'],Response::HTTP_OK);

        } else {
            ShopWishlist::create([
                "user_id" => auth()->user()->id,
                "product_id"=>$product->id,
            ]);

            return response()->json(['wishlist' => "Wishlist added", 'status'=>'true'],Response::HTTP_OK);
        }
    }

    public function wishlistProducts(Request $request) {
        $wishlists = ShopWishlist::where(["user_id"=>auth()->user()->id])->get();
        $x=0;
        $wish = array();

        foreach($wishlists as $wishlist) {
            $product = ShopProduct::with('descriptions')->with('images')->where('id', $wishlist->product_id)->first();
            if($product) {
                $wishlist->product = $product;
                $wish[] = $wishlist;
            }
            $x++;
        }
        return response()->json(['wishlist_products' => $wish, 'status'=>'true'],Response::HTTP_OK);
    }
    
    public function randomordernumber() {
        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 

        return substr(str_shuffle($str_result), 0, 6);
    }
}
