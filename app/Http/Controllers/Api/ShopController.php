<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Models\ShopCategory;
use App\Models\ShopCategoryDescription;
use App\Models\ShopProduct;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ShopCart;
use App\Models\ShopBanner;
use App\Models\ShopAdvertise;
use App\Models\ShopOrder;
use App\Models\ShopOrderDetail;
use App\Models\ShopTax;
use App\Models\ShopWishlist;

use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{

    public function banner()
    {
        $bannerLists = ShopBanner::all();
        foreach($bannerLists as $bannerList) {
            $bannerList->image = asset($bannerList->image);
        }
        return response()->json(['banner' => $bannerLists, 'status'=>'true'],Response::HTTP_OK);
    }

    public function advertise()
    {
        $advertiseLists = ShopAdvertise::all();
        foreach($advertiseLists as $advertiseList) {
            $advertiseList->image = asset($advertiseList->image);
        }
        return response()->json(['advertise' => $advertiseLists, 'status'=>'true'],Response::HTTP_OK);
    }

    public function category(Request $request) {
        $categoryLists = ShopCategory::with('descriptions')->get();
        foreach($categoryLists as $categoryList) {
            $categoryList->image = asset($categoryList->image);
        }

        return response()->json(['category' => $categoryLists, 'status'=>'true'],Response::HTTP_OK);
    }

    public function products(Request $request, $userid=null) {
        $productLists = ShopProduct::with('descriptions')->with('images')->with('categories')->get();
        foreach($productLists as $productList) {
            $productList->images[0]->image = asset($productList->images[0]->image);
            $productList->descriptions[0]->name = $productList->descriptions[0]->name.' - '.$productList->option_name;

            if($userid != null) {
                $carts = ShopCart::where(["user_id"=>$userid, "product_id"=>$productList->id])->first();
                if($carts) {
                    $productList->cart = "yes";
                } else {
                    $productList->cart = "no";
                }
            } else {
                $productList->cart = "no";
            }

            if($userid != null) {
                $wish = ShopWishlist::where(["user_id"=>$userid, "product_id"=>$productList->id])->first();
                if($wish) {
                    $productList->wish = "yes";
                } else {
                    $productList->wish = "no";
                }
            } else {
                $productList->wish = "no";
            }

        }

        return response()->json(['products' => $productLists, 'status'=>'true'],Response::HTTP_OK);
    }

    public function productsbycategory(Request $request, $id, $userid=null) {
        $productLists = ShopProduct::with('descriptions')->with('images')->with('categories')->whereHas('categories', function($q) use($id){
            $q->where(["category_id"=>$id]);
        })->get();

        foreach($productLists as $productList) {
            $productList->images[0]->image = asset($productList->images[0]->image);
            $productList->descriptions[0]->name = $productList->descriptions[0]->name.' - '.$productList->option_name;

            if($userid != null) {
                $carts = ShopCart::where(["user_id"=>$userid, "product_id"=>$productList->id])->first();
                if($carts) {
                    $productList->cart = "yes";
                } else {
                    $productList->cart = "no";
                }
            } else {
                $productList->cart = "no";
            }

            if($userid != null) {
                $wish = ShopWishlist::where(["user_id"=>$userid, "product_id"=>$productList->id])->first();
                if($wish) {
                    $productList->wish = "yes";
                } else {
                    $productList->wish = "no";
                }
            } else {
                $productList->wish = "no";
            }
        }

        return response()->json(['productsbycategory' => $productLists, 'status'=>'true'],Response::HTTP_OK);
    }

    public function productsbyid(Request $request, $id, $userid=null) {
        $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$id])->first();
        if($productList) {
            $productList->images[0]->image = asset($productList->images[0]->image);
        } else {
            $productList = "";
        }
        $productList->descriptions[0]->name = $productList->descriptions[0]->name.' - '.$productList->option_name;

        if($userid != null) {
            $carts = ShopCart::where(["user_id"=>$userid, "product_id"=>$productList->id])->first();
            if($carts) {
                $productList->cart = "yes";
            } else {
                $productList->cart = "no";
            }
        } else {
            $productList->cart = "no";
        }

        if($userid != null) {
            $wish = ShopWishlist::where(["user_id"=>$userid, "product_id"=>$productList->id])->first();
            if($wish) {
                $productList->wish = "yes";
            } else {
                $productList->wish = "no";
            }
        } else {
            $productList->wish = "no";
        }
        
        return response()->json(['product' => $productList, 'status'=>'true'],Response::HTTP_OK);
    }

    public function addtocart(Request $request) {
        $product_id = $request->product_id;
        $quantity = $request->quantity;

        $cart = ShopCart::where(["user_id"=>auth()->user()->id, "product_id"=>$product_id])->first();

        if($cart) {

            $cart->quantity = $cart->quantity + $quantity;
            $cart->save();

        } else {
            ShopCart::create([
                "user_id" => auth()->user()->id,
                "product_id"=>$product_id,
                "quantity"=>$quantity,
            ]);
        }
        return response()->json(['product' => "Product added in cart", 'status'=>'true'],Response::HTTP_OK);
    }

    public function removecart(Request $request, $id) {
        ShopCart::where(["id"=>$id])->delete();
        return response()->json(['product' => "Product remove from cart", 'status'=>'true'],Response::HTTP_OK);
    }

    public function updatecart(Request $request) {

        if($request->qty == 0) {
            ShopCart::where(["id"=>$request->id])->delete();
        } else {
            $cart = ShopCart::where(["id"=>$request->id])->first();
            $cart->quantity = $request->qty;
            $cart->save();
        }

        return response()->json(['product' => "Product updated in cart", 'status'=>'true'],Response::HTTP_OK);
    }

    public function cart() {
        $cartArray = array();
        $carts = ShopCart::where(["user_id"=>auth()->user()->id])->get();
        $final_price = 0;
        $final_tax = 0;
        $subtotal = 0;

        foreach($carts as $cart) {
            
            $checkproductList = ShopProduct::where(["id"=>$cart->product_id])->first();
            if($checkproductList) {
                $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$cart->product_id])->first();
                $productList->images[0]->image = asset($productList->images[0]->image);
                $productList->descriptions[0]->name = $productList->descriptions[0]->name.' - '.$productList->option_name;
    
                $cartArray[] = [
                    "id"=>$cart->id,
                    "name" => $productList->descriptions[0]->name,
                    "image" => $productList->images[0]->image,
                    "price" => $productList->price,
                    "sale_price" => $productList->sale_price,
                    "quantity" => $cart->quantity,
                    "total_price" => $productList->sale_price * $cart->quantity,
                ];
                
                $total = $productList->sale_price * $cart->quantity;
                
                $getTax = ShopTax::where('id',$productList->tax_id)->first();
                if($getTax) {
                    if($getTax->value > 0) {
                        $calTax = $getTax->value / 100 * $total;
                        $tax = round($calTax, 2);
                    } else {
                        $tax = 0;
                    }
                } else {
                    $tax = 0;
                }
                
                $subtotal = $subtotal + $total;
                $final_tax = $final_tax + $tax;
                $final_price = $final_price + $total + $tax;
            }
        }

        return response()->json(['cart' => $cartArray, 'sub_total'=>$subtotal, 'final_tax'=>$final_tax, 'final_amount'=>$final_price, 'status'=>'true'],Response::HTTP_OK);
    }

    public function checkout() {
        $cartArray = array();
        $carts = ShopCart::where(["user_id"=>auth()->user()->id])->get();
        $final_price = 0;
        $final_tax = 0;
        $subtotal = 0;
        
        foreach($carts as $cart) {

            $checkproductList = ShopProduct::where(["id"=>$cart->product_id])->first();
            if($checkproductList) {
                $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$cart->product_id])->first();
                $productList->images[0]->image = asset($productList->images[0]->image);
                $productList->descriptions[0]->name = $productList->descriptions[0]->name.' - '.$productList->option_name;
    
                $cartArray[] = [
                    "id"=>$cart->id,
                    "name" => $productList->descriptions[0]->name,
                    "image" => $productList->images[0]->image,
                    "price" => $productList->price,
                    "sale_price" => $productList->sale_price,
                    "quantity" => $cart->quantity,
                    "total_price" => $productList->sale_price * $cart->quantity,
                ];
                $total = $productList->sale_price * $cart->quantity;
    
                $getTax = ShopTax::where('id',$productList->tax_id)->first();
                if($getTax) {
                    if($getTax->value > 0) {
                        $calTax = $getTax->value / 100 * $total;
                        $tax = round($calTax, 2);
                    } else {
                        $tax = 0;
                    }
                } else {
                    $tax = 0;
                }
                $subtotal = $subtotal + $total;
                $final_tax = $final_tax + $tax;
                $final_price = $final_price + $total + $tax;
            }
        }

        if($final_price > 200 && $final_price < 300) {
            $deliverycharges = 15;
            $final_price = $final_price + $deliverycharges;
        } else {
            $deliverycharges = 0;
        }

        return response()->json(['checkout' => $cartArray, 'subtotal'=>$subtotal, 'deliverycharges'=>$deliverycharges, 'final_tax'=>$final_tax, 'final_price'=>$final_price, 'status'=>'true'],Response::HTTP_OK);
    }

    public function placeorder(Request $request) {

        $validator= Validator::make($request->all(),[
            'full_name'=>'required',
            'phone'=>'required|min:8',
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'zipcode'=>'required',
            'payment_mode'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
            'message' => $validator->errors()->first(),
            'status'=>'false',
            "placeOrder"=>(object)[]
            ],Response::HTTP_LENGTH_REQUIRED);
        }

        $cartArray = array();
        $carts = ShopCart::where(["user_id"=>auth()->user()->id])->get();
        $sub_price = 0;
        $total_tax = 0;
        $final_price = 0;

        foreach($carts as $cart) {

            $checkproductList = ShopProduct::where(["id"=>$cart->product_id])->first();
            if($checkproductList) {
                $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$cart->product_id])->first();
                $productList->images[0]->image = asset($productList->images[0]->image);
                $productList->descriptions[0]->name = $productList->descriptions[0]->name.' - '.$productList->option_name;
                
                $getTax = ShopTax::where('id', $productList->tax_id)->first();
                
                $total_price = $productList->sale_price * $cart->quantity;
                if($getTax) {
                    if($getTax->value > 0) {
                        $tax =  $total_price / $getTax->value * 100;
                    } else {
                        $tax =  0;
                    }
                } else {
                    $tax =  0;
                }
    
                $cartArray[] = [
                    "product_id"=>$cart->product_id,
                    "name" => $productList->descriptions[0]->name,
                    "price" => $productList->sale_price,
                    "qty" => $cart->quantity,
                    "total_price" => $total_price,
                    "tax" => $tax,
                    "sku" => $productList->sku,
                ];
    
                $sub_price = $sub_price + $total_price;
    
                $total_tax = $total_tax + $tax;
    
                $amount_with_tax = $total_price + $total_tax;
                $final_price = $final_price + $amount_with_tax;
            }
        }

        if($final_price > 200 && $final_price < 300) {
            $deliverycharges = 15;
            $final_price = $final_price + $deliverycharges;
        } else {
            $deliverycharges = 0;
        }

        $order = ShopOrder::create([
            "user_id" => auth()->user()->id,
            "order_number" => auth()->user()->id. $this->randomordernumber(),
            "subtotal" => $sub_price,
            "tax" => $total_tax,
            "total" => $final_price,
            "name" => $request->full_name,
            "address" => $request->address,
            "post_code" => $request->zipcode,
            "city" => $request->city,
            "state" => $request->state,
            "phone" => $request->phone,
            "comment" => $request->other_notes,
            "payment_method" => $request->payment_mode,
            "transaction"=> $request->payment_id
        ]);

        foreach($cartArray as $cartArr) {
            
            ShopOrderDetail::create([
                "order_id" => $order->id,
                "product_id"=>$cartArr['product_id'],
                "name" => $cartArr['name'],
                "price" => $cartArr['price'],
                "qty" => $cartArr['qty'],
                "total_price" => $cartArr['total_price'],
                "tax" => $cartArr['tax'],
                "sku" => $cartArr['sku'],
            ]);
        }

        ShopCart::where('user_id', auth()->user()->id)->delete();

        return response()->json(['message'=>"Order Placed Successfully",'status'=>'true'], Response::HTTP_OK);
    }

    public function randomordernumber() {
        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 

        return substr(str_shuffle($str_result), 0, 5);
    }

    public function orders(Request $request) {
        $orders = ShopOrder::where('user_id', auth()->user()->id)->with('details')->orderBy('id','DESC')->get();
        
        return response()->json(['orders'=>$orders, 'message'=>"Order Retrived Successfully",'status'=>'true'], Response::HTTP_OK);
    }

    public function orderdetail(Request $request, $id) {
        $order = ShopOrder::where('id', $id)->with('details')->first();
        foreach($order->details as $orderdetail) {
            $product = ShopProductImage::where('product_id', $orderdetail->product_id)->first();
            $orderdetail->image = $product->image; 
        }
        
        return response()->json(['orderdetail'=>$order, 'message'=>"Order Retrived Successfully",'status'=>'true'], Response::HTTP_OK);
    }

    public function cartheader() {

        if(auth()->check()) {
            $carts = \App\Models\ShopCart::where(["user_id"=>auth()->user()->id])->get();
            $totalSum = 0;
            $totalCount = 0;
            foreach($carts as $cart) {

                $product = \App\Models\ShopProduct::where(["id"=>$cart->product_id])->first();
                if($product) {
                    $totalSum = $totalSum + $product->sale_price * $cart->quantity;
                    $totalCount = $totalCount + $cart->quantity;
                }
            }

            $cartData = ["cartTotal"=>$totalCount, "total"=>$totalSum];
        } else {

            $cartData = ["cartTotal"=>0, "total"=>0.00];

        }
        return response()->json(['cartData'=>$cartData, 'message'=>"Cart Retrived",'status'=>'true'], Response::HTTP_OK);
    }
}
