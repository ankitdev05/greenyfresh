<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\Models\ShopCategory;
use App\Models\ShopCategoryDescription;
use App\Models\ShopProduct;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ShopCart;
use App\Models\ShopBanner;
use App\Models\ShopOrder;
use App\Models\ShopOrderDetail;
use App\Models\ShopTax;
use App\Models\ShopAdvertise;
use App\Models\Visitor;
use Auth;
use Session;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $productLists = ShopProduct::with('descriptions')->with('images')->with('categories')->orderBy('id','DESC')->limit(8)->get();
        $bannerLists = ShopBanner::all();
        foreach($bannerLists as $bannerList) {
            $bannerList->image = asset($bannerList->image);
        }

        $advertiseLists = ShopAdvertise::all();
        foreach($advertiseLists as $advertiseList) {
            $advertiseList->image = asset($advertiseList->image);
        }
        
        $currentIP = $this->getRealIpAddr();
        $visitor = Visitor::where("ip", $currentIP)->first();
        if($visitor) {} else {
            Visitor::create([
                "ip" => $currentIP,
                "counting" => 1 
            ]);
        }
        
        return view('web.index', compact('productLists', 'bannerLists', 'advertiseLists'));
    }
    
    function getRealIpAddr() {
        
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function login(Request $request)
    {
        if(auth()->check()) {
            return redirect()->route('shop');
        }

        if($request->isMethod('post')) {

            $this->validate($request, [
                'phone_no' =>"required",
                'password'=>'required',
            ]);

            $user = User::where('phone',$request->phone_no)->first();
            if(!$user){
                Session::flash('error', 'Login Failed.');
                return redirect()->back();
            }
            if($user->status !=1) {
                Session::flash('error', 'User is blocked');
                return redirect()->back();
            }

            if(Auth::guard('web')->attempt(['email' => $user->email, 'password' =>$request->password], $request->remember)) {

                return redirect()->back();

            } else {
                Session::flash('error', 'Login Failed.');
                return redirect()->back();
            }
        }

        return view('web.login');
    }

    public function register(Request $request)
    {
        if(auth()->check()) {
            return redirect()->route('shop');
        }

        if($request->isMethod('post')) {

            $this->validate($request, [
                'name'=>'required',
                'email'=>'required|email|unique:users,email',
                'phone'=>'required|min:8|unique:users,phone',
                'country_code'=>['required','numeric'],
                'password'=>'required|min:5|max:20|required_with:confirm_password|same:confirm_password',
                'confirm_password'=>'required',
                'address'=>'required',
                'gender'=>'required',
                'dob'=>'required',
            ]);

            $user = new User;
            $user->email = $request->email;
            $user->name = $request->name;
            $user->address = $request->address;
            $user->phone = $request->phone;
            $user->country_code = $request->country_code;
            $user->gender = $request->gender;
            $user->dob = $request->dob;
            $user->is_phone_verify ='1';
            $user->password = bcrypt($request->password);
            $user->save();

            Session::flash('success', 'Registration Successfully');
            return redirect()->route('login');
        }

        return view('web.register');
    }


    public function shop($alias=null)
    {
        if($alias) {
            $category = ShopCategory::where(["alias"=>$alias])->first();
            $id = $category->id;

            $productLists = ShopProduct::with('descriptions')->with('images')->with('categories')->whereHas('categories', function($q) use($id){
                $q->where(["category_id"=>$id]);
            })->orderBy('id','DESC')->get();

        } else {
            $productLists = ShopProduct::with('descriptions')->with('images')->with('categories')->orderBy('id','DESC')->get();
        }

        $latestProductListsone = ShopProduct::with('descriptions')->with('images')->with('categories')->orderBy('id','DESC')->limit(3)->get();
        $latestProductListstwo = ShopProduct::with('descriptions')->with('images')->with('categories')->orderBy('id','DESC')->skip(3)->take(3)->get();

        return view('web.shop', compact('productLists','latestProductListsone','latestProductListstwo'));
    }

    public function productsbyid(Request $request, $alias) {
        $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["alias"=>$alias])->first();
        $productList->images[0]->image = asset($productList->images[0]->image);

        $catid = $productList->categories[0]->id;
        $relatedProducts = $productLists = ShopProduct::with('descriptions')->with('images')->with('categories')->whereHas('categories', function($q) use($catid){
            $q->where(["category_id"=>$catid]);
        })->limit(4)->orderBy('id', 'DESC')->get();

        return view('web.shop_details', compact('productList', 'relatedProducts'));
    }

    public function searchproducts(Request $request) {
        $searching = $request->key;
        $idarray = array();
        $productids = ShopProductDescription::select('product_id')->orWhere('name','like',"%$searching%")->orWhere('description','like',"%$searching%")->orWhere('content','like',"%$searching%")->get();
        if($productids) {
            foreach($productids as $productid) {
                $idarray[] = $productid->product_id;
            }
        } else {
            $idarray[] = 0;
        }

        $productLists = ShopProduct::with('descriptions')->with('images')->with('categories')->whereIn('id', $idarray)->orderBy('id','DESC')->get();

        $latestProductListsone = ShopProduct::with('descriptions')->with('images')->with('categories')->orderBy('id','DESC')->limit(3)->get();
        $latestProductListstwo = ShopProduct::with('descriptions')->with('images')->with('categories')->orderBy('id','DESC')->skip(3)->take(3)->get();

        return view('web.shop', compact('productLists','latestProductListsone','latestProductListstwo'));

    }

}
