<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Razorpay\Api\Api;
use Session;
use Redirect;

use App\User;
use App\Models\ShopProduct;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ShopCart;
use App\Models\ShopOrder;
use App\Models\ShopOrderDetail;
use App\Models\ShopTax;
use App\Models\UserAddress;
use App\Models\TempOrder;
use Auth;
use GuzzleHttp\Client;
use PaytmWallet;

class PaymentController extends Controller
{    
    public function create() {

        $cartArray = array();
        $carts = ShopCart::where(["user_id"=>auth()->user()->id])->get();
        $final_price = 0;

        foreach($carts as $cart) {

            $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$cart->product_id])->first();
            $productList->images[0]->image = asset($productList->images[0]->image);

            $cartArray[] = [
                "id"=>$cart->id,
                "name" => $productList->descriptions[0]->name,
                "image" => $productList->images[0]->image,
                "price" => $productList->sale_price,
                "quantity" => $cart->quantity,
                "total_price" => $productList->sale_price * $cart->quantity,
            ];
            $final_price = (int)$final_price + $productList->sale_price * $cart->quantity;
        }

        $subtotal = $final_price;
        if($final_price > 200 && $final_price < 300) {
            $deliverycharges = 15;
            $final_price = $final_price + $deliverycharges;
        } else {
            $deliverycharges = 0;
        }

        $temp = TempOrder::where('user_id', auth()->user()->id)->first();
        $address = UserAddress::where('id', $temp->address_id)->first();
        
        if($temp->payment_type == "online") {
            return view('web.payWithRazorpay', compact('subtotal','deliverycharges','final_price','address'));
        
        } else if($temp->payment_type == "paypal") {
            
            $payment = PaytmWallet::with('receive');
            $payment->prepare([
              'order' => $temp->order_number,
              'user' => auth()->user()->id,
              'mobile_number' => auth()->user()->phone,
              'email' => auth()->user()->email,
              'amount' => $final_price,
              'callback_url' => 'http://greenyfresh.co.in/paypal_status'
            ]);
            return $payment->receive();
        }
    }

    public function payment(Request $request) {

        $input = $request->all();

        //dd($input);

        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));

        $payment = $api->payment->fetch($input['razorpay_payment_id']);

        if(count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount']));

                $temp = TempOrder::where('user_id', auth()->user()->id)->first();
                $temp->payment_id = $input['razorpay_payment_id'];
                $temp->response = json_encode($response);
                $temp->save();

                return redirect()->route('online_payment');

            } catch (\Exception $e) {
                //return  $e->getMessage();
                //\Session::put('error',$e->getMessage());
                
                Session::flash('success', 'Payment Failed');
                return redirect()->route('checkout');
            }
        } else {
            Session::flash('success', 'Payment Failed');
            return redirect()->route('checkout');
        }
    }
    
    // public function paypal_payment() {
        
    //     $payment = PaytmWallet::with('receive');
    //     $payment->prepare([
    //       'order' => $order->id,
    //       'user' => $user->id,
    //       'mobile_number' => $user->phonenumber,
    //       'email' => $user->email,
    //       'amount' => $order->amount,
    //       'callback_url' => 'http://example.com/payment/status'
    //     ]);
    //     return $payment->receive();
    // }
    
    /**
     * Obtain the payment information.
     *
     * @return Object
     */
    public function paymentCallback(Request $request) {
        
        $transaction = PaytmWallet::with('receive');
        
        $response = $transaction->response(); // To get raw response as array
        //Check out response parameters sent by paytm here -> http://paywithpaytm.com/developer/paytm_api_doc?target=interpreting-response-sent-by-paytm
        
        if($transaction->isSuccessful()) {
            //Transaction Successful
            
            $transaction->getResponseMessage(); //Get Response Message If Available
            //get important parameters via public methods
            $transaction->getOrderId(); // Get order id
            $trans_id = $transaction->getTransactionId(); // Get transaction id
            
            $temp = TempOrder::where('user_id', auth()->user()->id)->first();
            $temp->payment_id = $trans_id;
            $temp->response = json_encode($response);
            $temp->save();

            return redirect()->route('online_payment');
        
        }else if($transaction->isFailed()) {
          //Transaction Failed
          return redirect()->route('checkout');
        }else if($transaction->isOpen()) {
          //Transaction Open/Processing
          return redirect()->route('checkout');
        }
    }
    
    public function randomordernumber() {
        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 

        return substr(str_shuffle($str_result), 0, 5);
    }
}