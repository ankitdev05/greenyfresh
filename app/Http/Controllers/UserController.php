<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\Models\ShopCategory;
use App\Models\ShopCategoryDescription;
use App\Models\ShopProduct;
use App\Models\ShopProductCategory;
use App\Models\ShopProductDescription;
use App\Models\ShopProductImage;
use App\Models\ShopCart;
use App\Models\ShopBanner;
use App\Models\ShopOrder;
use App\Models\ShopOrderDetail;
use App\Models\ShopTax;
use App\Models\ShopAdvertise;
use App\Models\ShopWishlist;
use App\Models\UserAddress;
use App\Models\TempOrder;
use App\Models\ForgotPassword;
use Auth;
use Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReset;

class UserController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function logout() {

        if(Auth::guard('web')->check()) {
            Auth::guard('web')->logout();
            return redirect('/login');
        }
    }

    public function profile() {
        return view('web.profile');
    }

    public function update(Request $request) {
        //try {
            $this->validate($request, [
                'full_name'=>'required',
                'phone'=>'required|min:8',
                'country_code'=>['required','numeric'],
                'address'=>'required',
                'gender'=>'required',
                'dob'=>'required',
            ]);

            $user = User::find(auth()->user()->id);
            $user->name = $request->full_name;
            $user->address = $request->address;
            $user->phone = $request->phone;
            $user->country_code = $request->country_code;
            $user->gender = $request->gender;
            $user->dob = $request->dob;
            $user->save();

            Session::flash('success', 'User updated Successfully');
            return redirect()->back();

        // } catch (\Exception $e) {

        //     return redirect()->back();
        // }
    }

    public function password(Request $request) {
        return view('web.password');
    }

    public function updatepassword(Request $request) {
        //try {
            $this->validate($request, [
                'old_password'=>'required',
                'new_password'=>'required',
            ]);

            if(!Hash::check(request()->old_password, auth()->user()->password)){
                
                Session::flash('error', 'Old Password does not match');
                return redirect()->back();
            }

            $user = User::find(auth()->user()->id);
            $user->password = bcrypt($request->new_password);
            $user->save();

            Session::flash('success', 'Password Changed Successfully');
            return redirect()->back();

        // } catch (\Exception $e) {

        //     return redirect()->back();
        // }
    }
    
    public function forgotpassword(Request $request) {
        
        if($request->isMethod('post')) {
            $this->validate($request, [
                'email'=>'required',
            ]);
            
            $user = User::where('email', $request->email)->first();
            if($user) {
                Mail::to(request()->email)->send(new PasswordReset(request()->email));
                
                Session::flash('success', 'Password change verification link has been sent to your email id');
                return redirect()->back();
            } else {
                Session::flash('success', 'Email id does not exists');
                return redirect()->back();
            }
            
        } else {
            return view('web.forgot_password');
        }
    }
    
    public function reset(Request $request) {
        //try {
        
        if($request->isMethod('post')) {
            
            $this->validate($request, [
                'verify_code'=>'required',
                'email'=>'required',
                'new_password'=>'required',
            ]);

            $verify = ForgotPassword::where(["email"=>$request->email, "code"=>$request->verify_code, "status"=>1])->first();
            if($verify) {
                
                $user = User::where('email', $request->email)->first();
                if($user) {
                    $user->password = bcrypt($request->new_password);
                    $user->save();
                    
                    $verify->status = 2;
                    $verify->save();
        
                    Session::flash('success', 'Password Changed Successfully');
                    return redirect()->back();
                
                } else {
                    Session::flash('success', 'Details not match');
                    return redirect()->back();
                }
                
            } else {
                Session::flash('success', 'Details not match');
                return redirect()->back();
            }
            
        } else {
            return view('web.reset_password');
        }

        // } catch (\Exception $e) {

        //     return redirect()->back();
        // }
    }

    public function address() {
        $address = UserAddress::where('user_id', auth()->user()->id)->get();
        return view('web.user_address', compact('address'));
    }

    public function getaddress(Request $request) {
        $address = UserAddress::where('id', $request->id)->first();
        echo json_encode($address);
        die;
    }

    public function addAddress(Request $request) {
        //try {
            if($request->isMethod('post')) {
                $this->validate($request, [
                    'full_name'=>'required',
                    'phone'=>'required|min:8',
                    'post_code'=>['required','numeric'],
                    'address'=>'required',
                    'city'=>'required',
                    'state'=>'required',
                    'landmark'=>'required',
                ]);

                UserAddress::create([
                    "user_id" => auth()->user()->id,
                    "full_name" =>$request->full_name,
                    "postcode" =>$request->post_code, 
                    "address" =>$request->address, 
                    "city" =>$request->city, 
                    "state" =>$request->state, 
                    "phone" =>$request->phone, 
                    "landmark" =>$request->landmark, 
                ]);

                Session::flash('success', 'User Address Added Successfully');
                return redirect()->route('address');
            } else {
                return view('web.user_add_address');   
            }

        // } catch (\Exception $e) {

        //     return redirect()->back();
        // }
    }

    public function deleteAddress(Request $request, $id) {
        UserAddress::where(["id"=>$id])->delete();
        return redirect()->back();
    }

    public function wishlist(Request $request, $alias) {

        if(auth()->check()) {
            $alias = $alias;
            $product = ShopProduct::where('alias', $alias)->first();
            $wish = ShopWishlist::where(["user_id"=>auth()->user()->id, "product_id"=>$product->id])->first();

            if($wish) {

                $wish->delete();

            } else {
                ShopWishlist::create([
                    "user_id" => auth()->user()->id,
                    "product_id"=>$product->id,
                ]);
            }
        }

        return redirect()->back();
    }

    public function wishlistProducts(Request $request) {

        if(auth()->check()) {
            $wish = ShopWishlist::where(["user_id"=>auth()->user()->id])->get();
            $wishlists = array();

            foreach($wish as $wishlist) {
                $product = ShopProduct::with('descriptions')->with('images')->where('id', $wishlist->product_id)->first();
                if($product) {
                    $wishlist->product = $product;
                    $wishlists[] = $wishlist; 
                }
            }
            return view('web.wishlist', compact('wishlists'));
        
        } else {
            return redirect()->back();
        }
    }

    public function orders(Request $request) {
        $orders = ShopOrder::where('user_id', auth()->user()->id)->with('details')->orderBy('id','DESC')->get();
        return view('web.orders', compact('orders'));
    }

    public function orderdetails(Request $request, $id=null) {

        if(empty($id)) {
            return redirect()->route('orders');
        }

        $order = ShopOrder::where('user_id', auth()->user()->id)->with('details')->where('order_number', $id)->first();
        foreach($order->details as $orderdetail) {
            $product = ShopProductImage::where('product_id', $orderdetail->product_id)->first();
            $orderdetail->image = $product->image; 
        }
        
        return view('web.order_details', compact('order'));
    }

    public function addtocart(Request $request) {

        if(auth()->check()) {
            $alias = $request->product;
            $quantity = $request->quantity;

            $product = ShopProduct::where('alias', $alias)->first();
            $cart = ShopCart::where(["user_id"=>auth()->user()->id, "product_id"=>$product->id])->first();

            if($cart) {

                $cart->quantity = $cart->quantity + $quantity;
                $cart->save();

            } else {
                ShopCart::create([
                    "user_id" => auth()->user()->id,
                    "product_id"=>$product->id,
                    "quantity"=>$quantity,
                ]);
            }
        }
        return redirect()->back();
    }

    public function directaddtocart(Request $request, $alias) {
        
        if(auth()->check()) {
            $alias = $alias;
            $quantity = 1;

            $product = ShopProduct::where('alias', $alias)->first();

            $cart = ShopCart::where(["user_id"=>auth()->user()->id, "product_id"=>$product->id])->first();

            if($cart) {

                $cart->quantity = $cart->quantity + $quantity;
                $cart->save();

            } else {
                ShopCart::create([
                    "user_id" => auth()->user()->id,
                    "product_id"=>$product->id,
                    "quantity"=>$quantity,
                ]);
            }
        }
        return redirect()->back();
    }

    public function cart() {

        if(auth()->check()) {
            $cartArray = array();
            $carts = ShopCart::where(["user_id"=>auth()->user()->id])->get();
            $final_price = 0;
            $final_tax = 0;
            $subtotal = 0;
            
            foreach($carts as $cart) {

                $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$cart->product_id])->first();
                $productList->images[0]->image = asset($productList->images[0]->image);

                $cartArray[] = [
                    "id"=>$cart->id,
                    "name" => $productList->descriptions[0]->name,
                    "option_name" => $productList->option_name,
                    "image" => $productList->images[0]->image,
                    "price" => $productList->sale_price,
                    "quantity" => $cart->quantity,
                    "total_price" => $productList->sale_price * $cart->quantity,
                ];

                $total = $productList->sale_price * $cart->quantity;

                $getTax = ShopTax::where('id',$productList->tax_id)->first();
                if($getTax) {
                    if($getTax->value > 0) {
                        $calTax = $getTax->value / 100 * $total;
                        $tax = round($calTax, 2);
                    } else {
                        $tax = 0;
                    }
                } else {
                    $tax = 0;
                }
                $subtotal = $subtotal + $total;
                $final_tax = $final_tax + $tax;
                $final_price = $final_price + $total + $tax;
            }

            return view('web.cart', compact('cartArray','subtotal','final_price', 'final_tax'));
        
        } else {
            return redirect()->back();
        }
    }

    public function removecart(Request $request, $id) {
        ShopCart::where(["id"=>$id])->delete();
        return redirect()->back();
    }

    public function updatecart(Request $request) {

        if($request->qty == 0) {
            ShopCart::where(["id"=>$request->id])->delete();
        } else {
            $cart = ShopCart::where(["id"=>$request->id])->first();
            $cart->quantity = $request->qty;
            $cart->save();
        }

        return redirect()->back();
    }

    public function checkout() {
        $cartArray = array();
        $carts = ShopCart::where(["user_id"=>auth()->user()->id])->get();
        $final_price = 0;
        $final_tax = 0;
        $subtotal = 0;

        foreach($carts as $cart) {

            $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$cart->product_id])->first();
            $productList->images[0]->image = asset($productList->images[0]->image);

            $cartArray[] = [
                "id"=>$cart->id,
                "name" => $productList->descriptions[0]->name,
                "option_name" => $productList->option_name,
                "image" => $productList->images[0]->image,
                "price" => $productList->sale_price,
                "quantity" => $cart->quantity,
                "total_price" => $productList->sale_price * $cart->quantity,
            ];

            $total = $productList->sale_price * $cart->quantity;

            $getTax = ShopTax::where('id',$productList->tax_id)->first();
            if($getTax) {
                if($getTax->value > 0) {
                    $calTax = $getTax->value / 100 * $total;
                    $tax = round($calTax, 2);
                } else {
                    $tax = 0;
                }
            } else {
                $tax = 0;
            }
            $subtotal = $subtotal + $total;
            $final_tax = $final_tax + $tax;
            $final_price = $final_price + $total + $tax;
        }
        
        $address = UserAddress::where('user_id', auth()->user()->id)->get();

        if($final_price > 200 && $final_price < 300) {
            $deliverycharges = 15;
            $final_price = $final_price + $deliverycharges;
        } else {
            $deliverycharges = 0;
        }

        return view('web.checkout', compact('cartArray','subtotal','deliverycharges','final_price','address','final_tax'));
    }

    public function placeorder(Request $request) {

        $this->validate($request, [
            'full_name'=>'required',
            'phone'=>'required|min:8',
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'zipcode'=>'required',
            'landmark'=>'required',
            'payment_mode'=>'required',
            'orderamount'=>'required|gt:200',
        ]);

        if($request->payment_mode == "online" || $request->payment_mode == "paypal") {
            $selected_add = $request->selectedaddress;

            $temp = TempOrder::where('user_id', auth()->user()->id)->first();
            if($temp) {
                $temp->address_id = $selected_add;
                $temp->other_note = $request->other_notes;
                $temp->order_number = auth()->user()->id. $this->randomordernumber();
                $temp->payment_type = $request->payment_mode;
                $temp->save();

            } else {
                TempOrder::create(["user_id"=>auth()->user()->id, "order_number"=>auth()->user()->id. $this->randomordernumber(), "address_id"=>$selected_add, "payment_type"=>$request->payment_mode]);
            }
            return redirect()->route("payment-online");

        } else {

            $cartArray = array();
            $carts = ShopCart::where(["user_id"=>auth()->user()->id])->get();
            $sub_price = 0;
            $total_tax = 0;
            $final_price = 0;

            foreach($carts as $cart) {

                $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$cart->product_id])->first();
                $productList->images[0]->image = asset($productList->images[0]->image);
                $getTax = ShopTax::where('id', $productList->tax_id)->first();
                
                $total_price = $productList->sale_price * $cart->quantity;
                $tax =  $getTax->value / 100 * $total_price;

                $cartArray[] = [
                    "product_id"=>$cart->product_id,
                    "name" => $productList->descriptions[0]->name,
                    "price" => $productList->sale_price,
                    "qty" => $cart->quantity,
                    "total_price" => $total_price,
                    "tax" => $tax,
                    "sku" => $productList->sku,
                ];

                $sub_price = $sub_price + $total_price;

                $total_tax = $total_tax + $tax;

                $amount_with_tax = $total_price + $total_tax;
                $final_price = $final_price + $amount_with_tax;
            }

            if($final_price > 200 && $final_price < 300) {
                $deliverycharges = 15;
                $final_price = $final_price + $deliverycharges;
            } else {
                $deliverycharges = 0;
            }

            $order = ShopOrder::create([
                "user_id" => auth()->user()->id,
                "order_number" => auth()->user()->id. $this->randomordernumber(),
                "subtotal" => $sub_price,
                "tax" => $total_tax,
                "total" => $final_price,
                "name" => $request->full_name,
                "address" => $request->address,
                "post_code" => $request->zipcode,
                "city" => $request->city,
                "state" => $request->state,
                "landmark" => $request->landmark,
                "phone" => $request->phone,
                "comment" => $request->other_notes,
                "payment_method" => $request->payment_mode,
            ]);

            foreach($cartArray as $cartArr) {
                
                ShopOrderDetail::create([
                    "order_id" => $order->id,
                    "product_id"=>$cartArr['product_id'],
                    "name" => $cartArr['name'],
                    "price" => $cartArr['price'],
                    "qty" => $cartArr['qty'],
                    "total_price" => $cartArr['total_price'],
                    "tax" => $cartArr['tax'],
                    "sku" => $cartArr['sku'],
                ]);
            }

            ShopCart::where('user_id', auth()->user()->id)->delete();

            Session::flash('success', 'Order Placed Successfully');
            return redirect()->route('orders');
        }
    }

    public function onlinePlaceOrder() {

        $tempData = TempOrder::where('user_id', auth()->user()->id)->first();
        $address = UserAddress::where('id', $tempData->address_id)->first(); 

        $cartArray = array();
        $carts = ShopCart::where(["user_id"=>auth()->user()->id])->get();
        $sub_price = 0;
        $total_tax = 0;
        $final_price = 0;

        foreach($carts as $cart) {

            $productList = ShopProduct::with('descriptions')->with('images')->with('categories')->where(["id"=>$cart->product_id])->first();
            $productList->images[0]->image = asset($productList->images[0]->image);
            $getTax = ShopTax::where('id', $productList->tax_id)->first();
            
            $total_price = $productList->sale_price * $cart->quantity;
            $tax =  $total_price / $getTax->value * 100;

            $cartArray[] = [
                "product_id"=>$cart->product_id,
                "name" => $productList->descriptions[0]->name,
                "price" => $productList->sale_price,
                "qty" => $cart->quantity,
                "total_price" => $total_price,
                "tax" => $tax,
                "sku" => $productList->sku,
            ];

            $sub_price = $sub_price + $total_price;

            $total_tax = $total_tax + $tax;

            $amount_with_tax = $total_price + $total_tax;
            $final_price = $final_price + $amount_with_tax;
        }

        $order = ShopOrder::create([
            "user_id" => auth()->user()->id,
            "order_number" => $tempData->order_number,
            "subtotal" => $sub_price,
            "tax" => $total_tax,
            "total" => $final_price,
            "name" => $address->full_name,
            "address" => $address->address,
            "post_code" => $address->zipcode,
            "city" => $address->city,
            "state" => $address->state,
            "landmark" => $address->landmark,
            "phone" => $address->phone,
            "comment" => $tempData->other_note,
            "payment_method" => $tempData->payment_type,
            "transaction"=> $tempData->payment_id
        ]);

        foreach($cartArray as $cartArr) {
            
            ShopOrderDetail::create([
                "order_id" => $order->id,
                "product_id"=>$cartArr['product_id'],
                "name" => $cartArr['name'],
                "price" => $cartArr['price'],
                "qty" => $cartArr['qty'],
                "total_price" => $cartArr['total_price'],
                "tax" => $cartArr['tax'],
                "sku" => $cartArr['sku'],
            ]);
        }

        ShopCart::where('user_id', auth()->user()->id)->delete();
        TempOrder::where('user_id', auth()->user()->id)->delete();

        Session::flash('success', 'Order Placed Successfully');
        return redirect()->route('orders');
    }

    public function randomordernumber() {
        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 

        return substr(str_shuffle($str_result), 0, 5);
    }
    
    public function randompasswordcode() {
        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 

        return substr(str_shuffle($str_result), 0, 4);
    }

}
