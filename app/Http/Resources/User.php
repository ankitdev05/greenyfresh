<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'email'=>$this->email,
            'name'=>$this->name,           
            'phone' => $this->phone, 
            'country_code' => $this->country_code,   
            'address' => $this->address,   
            'gender' => $this->gender,   
            'dob' => $this->dob,  
            'referal_code' => $this->referal_code,  
            'is_phone_verify'=> $this->is_phone_verify, 
            'device_type'=> $this->device_type, 
            'device_token'=> $this->device_token, 
            'status'=> $this->status,
            'created_at'=> $this->created_at, 
            'updated_at'=> $this->status, 
             
            



        ];
    }
}
