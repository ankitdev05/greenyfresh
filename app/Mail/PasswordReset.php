<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\ForgotPassword;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    public $email_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($email)
    {
        $this->email_id = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $randnum = $this->random_code(5);

        ForgotPassword::create([
          'email'=>$this->email_id,
          'code'=>$randnum
        ]);

        $data['url'] = $randnum;
        return $this->view('emails.UserPasswordReset',$data);
    }

    function random_code($limit)
    {
      return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }
}
