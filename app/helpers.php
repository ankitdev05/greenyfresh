<?php

function categorylist() {
    return \App\Models\ShopCategory::with('descriptions')->get();
}

function cartheader() {

    if(auth()->check()) {
        $carts = \App\Models\ShopCart::where(["user_id"=>auth()->user()->id])->get();
        $totalSum = 0;
        $totalCount = 0;
        foreach($carts as $cart) {

            $product = \App\Models\ShopProduct::where(["id"=>$cart->product_id])->first();
            if($product) {
                $totalSum = $totalSum + $product->sale_price * $cart->quantity;
                $totalCount = $totalCount + $cart->quantity;
            } else {
                $totalSum = 0.00;
                $totalCount = 0;
            }
        }

        $cartData = ["cartTotal"=>$totalCount, "total"=>$totalSum];
    } else {

        $cartData = ["cartTotal"=>0, "total"=>0.00];

    }
    return $cartData;
}

function bannerlist() {
    return \App\Models\ShopBanner::where('id', 1)->first();
}

function headerlist() {
    return \App\Models\ShopPage::with('descriptions')->where(['status'=> 1])->whereIn('use_in', [1,3])->get();
}

function footerlist() {
    return \App\Models\ShopPage::with('descriptions')->where(['status'=> 1])->whereIn('use_in', [2,3])->get();
}
